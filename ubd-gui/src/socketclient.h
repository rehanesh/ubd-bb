#ifndef SOCKETCLIENT_H
#define SOCKETCLIENT_H

#include <QtCore>
#include <QString>
#include <QTcpSocket>
#include <log4qt/logger.h>

class SocketClient : public QTcpSocket
{
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    SocketClient(QString ip, QString port);
    ~SocketClient();

    QString ip();
    long port();
   // void SendTcpData(QString data);
    void SendTcpData(QByteArray data);
    QByteArray data();
    bool isConnected();
    void SetHostDisconnected();




public slots:
    void opens();
    void socketConnected();
    void serverTimeout();
    void socketDisconnected();

private slots:
    void handleSocketError(QAbstractSocket::SocketError error);

private:
    QString m_ip;
    long m_port;
    QTimer* heartbeatTimer;
    bool connected;

};

#endif // SOCKETCLIENT_H
