#include "keyboard.h"
 #include "log4qt/consoleappender.h"
#include "log4qt/logger.h"
#include "log4qt/ttcclayout.h"
#include "log4qt/logmanager.h"
#include <QApplication>
#include <QObject>
#include <QString>
#include <QByteArray>
#include <QtCore>
#include <log4qt/helpers/initialisationhelper.h>

void initLogger()
{
    QSettings s;
    s.clear();
    // Set logging level for Log4Qt to TRACE
    s.beginGroup("Log4Qt");
    s.setValue("Configuration", "../conf/log4qt.properties");
    s.endGroup();
    Log4Qt::Logger::logger("zdmv4_1")->info("Starting...");
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);

    initLogger();
    Keyboard keyboard;

    QObject::connect(&keyboard, SIGNAL(quit()), &app, SLOT(quit()));

    QObject::connect(&keyboard, SIGNAL(transmit(QByteArray)),
                     &keyboard, SLOT(handleKeyboardMessage(QByteArray)));

    return app.exec();
}
