#include "socketclient.h"
#include <QTimer>

SocketClient::SocketClient(QString ip, QString port)
{
    m_ip = ip;
    m_port = port.toLong();
    connected = false;
    connect(this, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(handleSocketError(QAbstractSocket::SocketError)));
    heartbeatTimer = new QTimer(this);
    connect(heartbeatTimer, SIGNAL(timeout()), this, SLOT(serverTimeout()));
    connect(this, SIGNAL(connected()), this, SLOT(socketConnected()));
    connect(this, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));

 }

SocketClient::~SocketClient()
{
    logger()->info("In Destructor ");
    if (this->isOpen())
    {
        this->flush();
        this->close();
    }
}

QString SocketClient::ip()
{
    return m_ip;
}
bool SocketClient::isConnected(){
  return connected;
}
void SocketClient::SetHostDisconnected(){
   connected = false;
}

long SocketClient::port()
{
    return m_port;
}

QByteArray SocketClient::data()
{
    QByteArray data;
    if (!this->isOpen())
    {
        logger()->error("%1:%2 is not connected.",
                        m_ip, QString::number(m_port));
        opens();
    }
    if (this->bytesAvailable() != 0)
    {
         data.resize(this->bytesAvailable());
         this->read(data.data(), data.size());
         logger()->error("Starting a HRTBEAT timer");
         heartbeatTimer->start(5000);
         connected = true;

    }
    logger()->info("GUI  RECEIVED  %1",
                     QString(data));


    return data;
}

void SocketClient::socketConnected (){
     logger()->error("Socket Connected. Starting a Heartbeat Timer");
     heartbeatTimer->start(5000);
     connected = true;

}

void SocketClient::opens()
{
    logger()->info("connecting %1:%2 ...", m_ip, QString::number(m_port));
    this->abort();
    this->connectToHost(m_ip, m_port);
}

void SocketClient::handleSocketError(QAbstractSocket::SocketError error)
{
    switch (error)
    {
    case QAbstractSocket::HostNotFoundError:
        logger()->error("remote host not found.");
        break;
    case QAbstractSocket::ConnectionRefusedError:
        logger()->error("connection refused by remote host.");
        break;
    case QAbstractSocket::RemoteHostClosedError:
        logger()->error("connection closed by remote host.");
        break;
    default:
        logger()->error("unkonwn error %1", this->errorString());
    }
    logger()->error("Retrying......");
    connected = false;

    QTimer::singleShot(2000, this, SLOT(opens()));

}

//void SocketClient::SendTcpData(QString data){

//   QByteArray buffer;
//   buffer = buffer.append(data);

//   logger()->debug("Sending %1", data);

//    this->write(buffer);
//    this->flush();
//}

void SocketClient::SendTcpData(QByteArray data){

 //  QByteArray buffer;
   // buffer = buffer.append(data);
    logger()->info("REHANESH SENDING  FROM KEYBOARD %1, %2",
                   QString(data.toHex()), QString(data));
   //logger()->debug("Sending %1", data);

    this->write(data);
    this->flush();
}

void SocketClient::serverTimeout () {
    logger()->error("HeartBeat Timer Expired");
    //this->disconnectFromHost();
    connected = false;
     heartbeatTimer->stop();

}

void SocketClient::socketDisconnected (){
//    logger()->error("Client Disconnected");
//     logger()->error("Stopping  Heart Beat Timer");
//    heartbeatTimer->stop();
//    connected = false;
//    this->opens();

}


