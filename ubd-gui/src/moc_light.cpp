/****************************************************************************
** Meta object code from reading C++ file 'light.h'
**
** Created: Mon Jul 15 05:27:13 2019
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "light.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'light.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Light[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       4,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
       6,   34,   38,   38, 0x05,
      39,   62,   38,   38, 0x05,

 // slots: signature, parameters, type, tag, flags
      68,   34,   38,   38, 0x08,
      88,   38,   38,   38, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Light[] = {
    "Light\0messageReceived(QByteArray)\0msg\0"
    "\0noResponse(QByteArray)\0daddr\0"
    "receive(QByteArray)\0reTransmit()\0"
};

void Light::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Light *_t = static_cast<Light *>(_o);
        switch (_id) {
        case 0: _t->messageReceived((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 1: _t->noResponse((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 2: _t->receive((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->reTransmit(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Light::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Light::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Light,
      qt_meta_data_Light, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Light::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Light::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Light::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Light))
        return static_cast<void*>(const_cast< Light*>(this));
    return QObject::qt_metacast(_clname);
}

int Light::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 4)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 4;
    }
    return _id;
}

// SIGNAL 0
void Light::messageReceived(const QByteArray & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Light::noResponse(const QByteArray & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
