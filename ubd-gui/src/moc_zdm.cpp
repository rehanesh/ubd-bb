/****************************************************************************
** Meta object code from reading C++ file 'zdm.h'
**
** Created: Mon Jul 15 05:26:49 2019
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "zdm.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'zdm.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Zdm[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      14,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       3,       // signalCount

 // signals: signature, parameters, type, tag, flags
       4,   21,   25,   25, 0x05,
      26,   21,   25,   25, 0x05,
      48,   68,   25,   25, 0x05,

 // slots: signature, parameters, type, tag, flags
      70,   21,   25,   25, 0x0a,
     104,   21,   25,   25, 0x0a,
     135,   21,   25,   25, 0x08,
     165,   21,  186,   25, 0x08,
     190,   21,   25,   25, 0x08,
     230,   21,   25,   25, 0x08,
     265,   21,   25,   25, 0x08,
     298,  329,   25,   25, 0x08,
     335,   21,   25,   25, 0x08,
     369,   25,   25,   25, 0x08,
     385,   25,   25,   25, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Zdm[] = {
    "Zdm\0display(QString)\0msg\0\0"
    "displayError(QString)\0displayLed(int,int)\0"
    ",\0handleKeyboardMessage(QByteArray)\0"
    "handleErrorMessage(QByteArray)\0"
    "handleHostMessage(QByteArray)\0"
    "handleF9(QByteArray)\0int\0"
    "handleNextSiblingZdmMessage(QByteArray)\0"
    "handlePowerbaseMessage(QByteArray)\0"
    "handleScannerMessage(QByteArray)\0"
    "handleLightMessage(QByteArray)\0msgin\0"
    "handleLightNoResponse(QByteArray)\0"
    "sendResetTest()\0HostisNotAlive()\0"
};

void Zdm::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Zdm *_t = static_cast<Zdm *>(_o);
        switch (_id) {
        case 0: _t->display((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 1: _t->displayError((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 2: _t->displayLed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 3: _t->handleKeyboardMessage((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 4: _t->handleErrorMessage((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 5: _t->handleHostMessage((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 6: { int _r = _t->handleF9((*reinterpret_cast< const QByteArray(*)>(_a[1])));
            if (_a[0]) *reinterpret_cast< int*>(_a[0]) = _r; }  break;
        case 7: _t->handleNextSiblingZdmMessage((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 8: _t->handlePowerbaseMessage((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 9: _t->handleScannerMessage((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 10: _t->handleLightMessage((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 11: _t->handleLightNoResponse((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 12: _t->sendResetTest(); break;
        case 13: _t->HostisNotAlive(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Zdm::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Zdm::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Zdm,
      qt_meta_data_Zdm, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Zdm::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Zdm::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Zdm::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Zdm))
        return static_cast<void*>(const_cast< Zdm*>(this));
    return QObject::qt_metacast(_clname);
}

int Zdm::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 14)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 14;
    }
    return _id;
}

// SIGNAL 0
void Zdm::display(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Zdm::displayError(const QString & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}

// SIGNAL 2
void Zdm::displayLed(int _t1, int _t2)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)), const_cast<void*>(reinterpret_cast<const void*>(&_t2)) };
    QMetaObject::activate(this, &staticMetaObject, 2, _a);
}
QT_END_MOC_NAMESPACE
