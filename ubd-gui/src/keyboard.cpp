#include "keyboard.h"
#include "ui_keyboard.h"
 #include "stdio.h"

QColor barColor(51, 204, 102);
QColor barColorV(66, 75, 244);
QColor orange(244, 108, 4);
QPen gridPen;
QPen gridPenV;
QVector<qreal> dashes;
QColor gridColor = barColor.darker();
QColor gridColorV = barColorV.darker();

QBrush redBrush(Qt::red);
QBrush blueBrush(Qt::blue);
QBrush greenBrush(Qt::green);
QBrush yellowBrush(Qt::yellow);
QBrush grayBrush(Qt::gray);
QBrush orangeBrush(orange);
QPen blackPen(Qt::black);

QBitArray blinkState(50, false);
QBitArray blinkSled(50, false);
unsigned char butColour[50];

QString greyBut = "QPushButton {"
                  "color: black;"
                  "border: 1px solid black;"
                  "padding: 0px;"
                  "background: #e1e1e1;"
                  "max-width: 28px;"
                  "max-height: 28px;}";
QString redBut =  "QPushButton {"
                  "color: black;"
                  "border: 1px solid black;"
                  "padding: 0px;"
                  "background: red;"
                  "max-width: 28px;"
                  "max-height: 28px;}";
QString greenBut =  "QPushButton {"
                  "color: black;"
                  "border: 1px solid black;"
                  "padding: 0px;"
                  "background: green;"
                  "max-width: 28px;"
                  "max-height: 28px;}";

Keyboard::Keyboard(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::Keyboard)
{
    ui->setupUi(this);

    gridPen.setWidth(2);
    gridPenV.setWidth(2);
    dashes << 2 << 2;
    gridPen.setDashPattern(dashes);
    gridPen.setColor(gridColor);
    gridPenV.setDashPattern(dashes);
    gridPenV.setColor(gridColorV);
    blackPen.setWidth(1);

    m_numberals = false;
    ui->button_numerals->setCheckable(true);

    QObject::connect(ui->button_numerals, SIGNAL(toggled(bool)),
                     this, SLOT(buttonNumberalsToggled(bool)));
    QObject::connect(ui->button_1, SIGNAL(clicked()),
                this, SLOT(button1Clicked()));
    QObject::connect(ui->button_2, SIGNAL(clicked()),
                this, SLOT(button2Clicked()));
    QObject::connect(ui->button_3, SIGNAL(clicked()),
                this, SLOT(button3Clicked()));
    QObject::connect(ui->button_4, SIGNAL(clicked()),
                this, SLOT(button4Clicked()));
    QObject::connect(ui->button_5, SIGNAL(clicked()),
                this, SLOT(button5Clicked()));
    QObject::connect(ui->button_6, SIGNAL(clicked()),
                this, SLOT(button6Clicked()));
    QObject::connect(ui->button_7, SIGNAL(clicked()),
                this, SLOT(button7Clicked()));
    QObject::connect(ui->button_8, SIGNAL(clicked()),
                this, SLOT(button8Clicked()));
    QObject::connect(ui->button_9, SIGNAL(clicked()),
                this, SLOT(button9Clicked()));
    QObject::connect(ui->button_0, SIGNAL(clicked()),
                this, SLOT(button0Clicked()));
    QObject::connect(ui->button_cancel, SIGNAL(clicked()),
                this, SLOT(buttonCancelClicked()));
    QObject::connect(ui->button_enter, SIGNAL(clicked()),
                this, SLOT(buttonEnterClicked()));
    QObject::connect(ui->action_quit, SIGNAL(triggered()),
                 this, SLOT(actionQuitTriggered()));
//    QObject::connect(ui->action_fullscreen, SIGNAL(triggered()),
//                this, SLOT(actionFullscreenTriggered()));
//    QObject::connect(ui->actionEnable_Maintenance_Mode, SIGNAL(triggered()),
//                this, SLOT(actionEnable_Maintenance_Mode()));

    scene = new QGraphicsScene();
//    subScene = new QGraphicsScene();
    view = new QGraphicsView();
//    subView = new QGraphicsView();
    scene->addWidget(this);


    QFile file(CONFIG_FILE_NAME);
    QDomDocument doc;

    if (!file.open(QIODevice::ReadOnly))
    {
        logger()->error("cannot open config file.");
        return;
    }
    if (!doc.setContent(&file))
    {
         logger()->error("cannot load config file to dom.");
        file.close();
        return;
    }
    file.close();
    QDomElement e = doc.documentElement();
    if (!e.isNull())
    {
        ParseXML(e);

    }
    int pos = m_port.indexOf(":");
    if (pos <= 0)
    {
        logger()->error("config.xml: device port wrong format.");
        return;
    }



    //setting up tcp client
   //tcp client to connect to server
    m_pSocket = new SocketClient(m_port.left(pos),
                                 m_port.mid(pos+1));
    m_pSocket->opens();
    QObject::connect(m_pSocket, SIGNAL(readyRead()), this, SLOT(readHostMessage()));


    blinkTimer = new QTimer(this);
    connect(blinkTimer, SIGNAL(timeout()), this, SLOT(upDate()));

#if 0
    QSignalMapper *signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(digitClicked(int)));
    int hoffset;
    bool flipSide = false;
   // if (Config::instance()->zdm()->flip_layout() == "yes")
        //flipSide = true;
    if (flipSide)
        hoffset =24;
    else
        hoffset =0;

    int voffset =50;
    for (int x=0; x<50; x++){
        QString text = QString::number(x+1);
        btnuser[x] = new QPushButton(text, this);
        if (x == 25){
            if (flipSide)
                hoffset = 25;
            else
                hoffset = 1;
            voffset = 0;
        }
        if (x > 25)
            hoffset += 2;
        if (flipSide)
            btnuser[x]->setGeometry(QRect(2 + (hoffset-x)*32 ,100+voffset,29,29));
        else
            btnuser[x]->setGeometry(QRect(2 + (x-hoffset)*32 ,100+voffset,29,29));

        btnuser[x]->setStyleSheet(greyBut);
        int mod = x%5;
        if(mod == 0 && x < 24 && x > 1)
            scene->addLine(x*32,80,x*32,200, gridPenV);

        QObject::connect(btnuser[x], SIGNAL(clicked()),signalMapper,SLOT(map()));
        signalMapper->setMapping(btnuser[x], x);
        scene->addWidget(btnuser[x]);
    }
#endif


    view->setScene(scene);
    view->show();
//    view->rotate(270);
    view->showFullScreen();
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
//    this->showFullScreen();
    this->activateWindow();
}

Keyboard::~Keyboard()
{
    delete ui;
    ui = 0;
}

void Keyboard::displayLed(int addr, int ledState){
    qDebug() << "Led Addr = " << addr << " Led State = " << ledState;
    bool blinkTest = false;
    if (ledState == 1){
        blinkSled[addr - 1] = true;
        butColour[addr - 1] = 1;
    }else if (ledState == 2) {
        blinkSled[addr - 1] = true;
        butColour[addr - 1] = 2;
    }else {
        blinkSled[addr - 1] = false;
        butColour[addr - 1] = 0;
    }
    for (int x = 0; x < 50; x++){
        if(blinkSled[x]){
            blinkTest = true;
        }
    }
    if(blinkTest)
        blinkTimer->start(255);

}
void Keyboard::upDate(){
    bool blinkTest = false;
    for (int x = 0; x < 50; x++){
        if(blinkSled[x]){
            blinkTest = true;
            if(blinkState[x]){
                btnuser[x]->setStyleSheet(greyBut);
            }else{
                if (butColour[x] == 1)
                    btnuser[x]->setStyleSheet(greenBut);
                else if (butColour[x] == 2)
                    btnuser[x]->setStyleSheet(redBut);
            }
            blinkState[x] = !blinkState[x];
        }else{
            btnuser[x]->setStyleSheet(greyBut);
        }
    }
    if(!blinkTest)
        blinkTimer->stop();

}
void Keyboard::digitClicked(int l){
    qDebug() << "BtnPress... " << l;

/*
    bool blinkTest = false;
    blinkSled[l] = !blinkSled[l];
    for (int x = 0; x < 50; x++){
        if(blinkSled[x]){
            butColour[x] = 1;
            blinkTest = true;
        }
    }
    if(blinkTest)
        blinkTimer->start(255);
*/
}
void Keyboard::displayMessage(const QString &msg)
{
    QString display_string;
    QRegExp rx("(\\|)"); //RegEx for '|'
    QStringList string_list = msg.split(rx);
    if(string_list.size() > 1){
    //string_list.at(0)  is START
     //string_list.at(3)  is END
     display_string = string_list.at(1);
    }else
        display_string = string_list.at(0);


    ui->label_displaymessage->setText(display_string);
}

void Keyboard::displayErrorMessage(const QString &msg)
{
    QRegExp rx("(\\|)"); //RegEx for '|'
    QStringList string_list = msg.split(rx);

    //string_list.at(0)  is START
     //string_list.at(3)  is END
    QString display_string = string_list.at(1);
     ui->label_displayerror->setText(display_string);
}

void Keyboard::buttonNumberalsToggled(bool checked)
{
    m_numberals = checked;
}

void Keyboard::button1Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('1');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x53, 0x54};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));

        displayMessage("STX|" + ui->label_start->text() +"|ETX");
        m_inputs.clear();
    }
}

void Keyboard::button2Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('2');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x53, 0x53};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_suspend->text());
        m_inputs.clear();
    }
}

void Keyboard::button3Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('3');
        displayMessage(QString(m_inputs));
    }
    else
    {
        displayMessage("");
        m_inputs.clear();
    }
}

void Keyboard::button4Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('4');
        displayMessage(QString(m_inputs));
    }
    else
    {
        displayMessage("");
        m_inputs.clear();
    }
}

void Keyboard::button5Clicked()
{
    if (m_numberals)
    {
        m_inputs.append("5");
        displayMessage(QString(m_inputs));
    }
    else
    {
        //unsigned char data[] = {0xfd, 0x54, 0x4d};
        unsigned char data[] = {0xfd, 0x54, 0x53};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_test->text());
        m_inputs.clear();
    }
}

void Keyboard::button6Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('6');
        displayMessage(QString(m_inputs));
    }
    else
    {
        displayMessage("");
        m_inputs.clear();
    }
}

void Keyboard::button7Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('7');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x55, 0x4c};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_unload->text());
        m_inputs.clear();
    }

}

void Keyboard::button8Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('8');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x52, 0x4c};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_relight->text());
        m_inputs.clear();
    }
}

void Keyboard::button9Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('9');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x52, 0x50};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_reprn->text());
        m_inputs.clear();
    }
}

void Keyboard::button0Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('0');
        displayMessage(QString(m_inputs));
    }
    else
    {
        //unsigned char data[] = {0xfd, 0x50, 0x44};
        unsigned char data[] = {0xfd, 0x44, 0x50};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_product->text());
        m_inputs.clear();
    }
}

void Keyboard::buttonCancelClicked()
{
    if (m_numberals)
    {
        m_numberals = false;
        ui->button_numerals->setChecked(false);
        m_inputs.clear();
        displayMessage("");
    }
    else
    {
        // cancel TEST mode -- AB
        unsigned char data[] = {0xfd, 0x41, 0x42};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->button_cancel->text());
        m_inputs.clear();
    }
}

void Keyboard::buttonEnterClicked()
{
    if (!m_inputs.isEmpty())
    {
        unsigned char data[] = {0xf0};
        QByteArray msg = QByteArray::fromRawData(
                    (char*)data, sizeof(data));
        msg.append(m_inputs);
        emit transmit(msg);
        displayMessage("");
        m_inputs.clear();
        ui->button_numerals->setChecked(false);
        m_numberals = false;
    }
}

void Keyboard::actionQuitTriggered()
{
    loginDialog = new LoginDialog();
    connect(loginDialog, SIGNAL(onloginFinished(QString)), this, SLOT(onDialogFinished(QString)));
    loginDialog->exec();
    delete loginDialog;
}

void Keyboard::onDialogFinished(QString value){
    if ( value == "123456" )
            emit quit();
}

void Keyboard::handleKeyboardMessage(const QByteArray& msg)
{
    logger()->info("RECEIVED FROM KEYBOARD %1, %2",
                   QString(msg.toHex()), QString(msg));

    if (!msg.isEmpty())
    {
        QByteArray cs;
        cs.append(m_stxchar);
        //cs.append(m_zdmaddr);
        cs.append(msg);
        cs.append(m_etxchar);
        logger()->info("Sending MESSAGE hex:%1, ascii:%2",
                       QString(cs.toHex()), QString(cs));

        m_pSocket->SendTcpData(cs);

    }
}
void Keyboard::readHostMessage()
{
    QByteArray data;
    while (true)
    {
        if (m_pSocket)
        {
            data = m_pSocket->data();
        }

        if (!data.isEmpty())
        {
            string_buf = string_buf + data;
            logger()->debug("GUI RECEIVED %1", QString(data));
        }
        else
        {
            break;
        }
    }
 QByteArray msg ;
 QString string_msg;
 logger()->debug("GUI RECEIVED %1",string_buf);

 while (!string_buf.isEmpty())
 {

     int startPos = string_buf.indexOf("STX");
     int endPos = string_buf.indexOf("ETX");

     logger()->debug("START Pos %1",startPos);
     logger()->debug("END Pos %1",endPos);

     if (startPos < 0)
     {
         logger()->warn("no START found in %1.",string_buf);
         break;
     }
     if (endPos < 0)
     {
         logger()->warn("no End found in %1.",string_buf);
         break;
     }

     int len = 0;
     len = endPos - startPos + 3;
     string_msg = string_buf.left(len);
     string_buf.remove(0, len);
     logger()->warn("%1 received. remaining %2.",
                    string_msg, string_buf);

//     //msg.append(string_msg);
//     //if msg Contains DspMsg

//     if(string_msg.contains("DspMsg")) handle_DspMsg(string_msg);
//     //if msg Contains DspList
//      else if (string_msg.contains("DspList")) handle_DspList(string_msg);
//     else if (string_msg.contains("HEARTBEAT")) {
//         itemHost->setBrush(brushGreen);
//         itemHost->setPen(penGreen);
//     }else if (string_msg.contains("RESET")) {
//         m_pSocket->SetHostDisconnected();
//         itemHost->setBrush(brushRed);
//         itemHost->setPen(penRed);
//         DisplayHomeScreen();
//     }
     displayMessage(string_msg);
  }
     return;
}
void Keyboard::ParseXML(QDomElement e){

    if (!e.isNull())
    {
        QDomElement host = e.firstChildElement("host");
        if (!host.isNull())
        {
             QDomElement device = host.firstChildElement("device");
             m_type = device.attribute("type").trimmed();
             QDomElement childe = device.firstChildElement(QString("port"));
             if (!childe.isNull())
             {
                 m_port = childe.text().trimmed().toLatin1();
             }
             childe = device.firstChildElement(QString("stxchar"));
             if (!childe.isNull())
             {
                 m_stxchar = parseToNumber(
                         childe.text().trimmed().toStdString().c_str()) & 0xff;
                 logger()->debug("stxchar %1", m_stxchar);

             }
             childe = device.firstChildElement(QString("etxchar"));
             if (!childe.isNull())
             {
                 m_etxchar = parseToNumber(
                             childe.text().trimmed().toStdString().c_str()) & 0xff;
                 logger()->debug("etxchar %1", m_etxchar);
             }

             childe = device.firstChildElement(QString("zdmaddr"));
             if (!childe.isNull())
             {
                 m_zdmaddr = childe.text().trimmed().toLong();
                 logger()->debug("ZDMaddr %1", m_zdmaddr);

             }
        }

}
    return;
}
int Keyboard::parseToNumber(const char* str)
{
    int val;
    int sign;
    int c;

    if(*str == '-') {
        sign = -1;
        str++;
    } else sign = 1;

    val = 0;

    /* check for hex */
    if(str[0] == '0' && (str[1] == 'x' || str[1] == 'X') ) {
        str += 2;
        while (1) {
            c = *str++;
            if(c >= '0' && c <= '9')
                val = (val<<4) + c - '0';
            else if (c >= 'a' && c <= 'f')
                val = (val<<4) + c - 'a' + 10;
            else if (c >= 'A' && c <= 'F')
                val = (val<<4) + c - 'A' + 10;
            else
                return val*sign;
        }
    }
    /* check for character */
    if (str[0] == '\'') {
        return sign * str[1];
    }

    /* assume decimal */
    while (1) {
        c = *str++;
        if(c <'0' || c > '9')
            return val*sign;
        val = val*10 + c - '0';
    }
    return 0;
}
