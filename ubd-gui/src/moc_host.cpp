/****************************************************************************
** Meta object code from reading C++ file 'host.h'
**
** Created: Mon Jul 15 05:26:55 2019
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "host.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'host.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Host[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
       7,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
       5,   33,   37,   37, 0x05,
      38,   33,   37,   37, 0x05,

 // slots: signature, parameters, type, tag, flags
      64,   33,   37,   37, 0x08,
      84,   37,   37,   37, 0x08,
      95,   37,   37,   37, 0x08,
     108,   37,   37,   37, 0x08,
     119,   37,   37,   37, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Host[] = {
    "Host\0messageReceived(QByteArray)\0msg\0"
    "\0errorReceived(QByteArray)\0"
    "receive(QByteArray)\0transmit()\0"
    "reTransmit()\0checkCTS()\0test()\0"
};

void Host::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Host *_t = static_cast<Host *>(_o);
        switch (_id) {
        case 0: _t->messageReceived((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 1: _t->errorReceived((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 2: _t->receive((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 3: _t->transmit(); break;
        case 4: _t->reTransmit(); break;
        case 5: _t->checkCTS(); break;
        case 6: _t->test(); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Host::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Host::staticMetaObject = {
    { &QObject::staticMetaObject, qt_meta_stringdata_Host,
      qt_meta_data_Host, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Host::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Host::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Host::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Host))
        return static_cast<void*>(const_cast< Host*>(this));
    return QObject::qt_metacast(_clname);
}

int Host::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QObject::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 7)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 7;
    }
    return _id;
}

// SIGNAL 0
void Host::messageReceived(const QByteArray & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Host::errorReceived(const QByteArray & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 1, _a);
}
QT_END_MOC_NAMESPACE
