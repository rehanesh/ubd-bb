#include "log.h"
#include <QFile>
#include <QTextStream>

log::log(QObject* parent) : QObject(parent)
{

}
log::~log()
{
}


void log::writeLog(QString info, QString type)
{
//    QDir directory(logPath);
//    QString filename = directory.absoluteFilePath(ui->logName->text() + ".log");
    QFile file( logAbsolutePath );
    if ( file.open(QIODevice::WriteOnly | QIODevice::Append) )
    {
        QTextStream stream( &file );
        QDate date = QDate::currentDate(); QString dateString = date.toString();
        QTime time = QTime::currentTime(); QString timeString = time.toString();
        stream << type << " " << dateString << " " << timeString << " " << info << endl;
        file.close();
    }
}
