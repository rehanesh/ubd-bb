#ifndef DISPLAY_H
#define DISPLAY_H

#include "keyboard.h"
#include <QObject>

class display : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(display)
public:
    explicit display(QObject* parent = 0);
    ~display();
    void errorMessage(const QString &msg);

signals:
    void displayError(const QString &msg);

private:

};

#endif // DISPLAY_H
