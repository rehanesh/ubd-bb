#include "logindialog.h"
#include "ui_logindialog.h"

LoginDialog::LoginDialog(QWidget *parent) :
    QDialog(parent),
    ui(new Ui::LoginDialog)
{
    ui->setupUi(this);
    signalMapper = new QSignalMapper(this);

    signalMapper->setMapping(ui->panelButton_1, QString("1"));
    signalMapper->setMapping(ui->panelButton_2, QString("2"));
    signalMapper->setMapping(ui->panelButton_3, QString("3"));
    signalMapper->setMapping(ui->panelButton_4, QString("4"));
    signalMapper->setMapping(ui->panelButton_5, QString("5"));
    signalMapper->setMapping(ui->panelButton_6, QString("6"));
    signalMapper->setMapping(ui->panelButton_7, QString("7"));
    signalMapper->setMapping(ui->panelButton_8, QString("8"));
    signalMapper->setMapping(ui->panelButton_9, QString("9"));
    signalMapper->setMapping(ui->panelButton_star, QString("*"));
    signalMapper->setMapping(ui->panelButton_0, QString("0"));
    signalMapper->setMapping(ui->panelButton_hash, QString("#"));

    connect(ui->panelButton_1, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_2, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_3, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_4, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_5, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_6, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_7, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_8, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_9, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_star, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_0, SIGNAL(clicked()), signalMapper, SLOT(map()));
    connect(ui->panelButton_hash, SIGNAL(clicked()), signalMapper, SLOT(map()));

    connect(signalMapper, SIGNAL(mapped(QString)), this, SLOT(process_Num(QString)));
}

LoginDialog::~LoginDialog()
{
    delete ui;
}

void LoginDialog::on_pushButton_Login_clicked()
{
//    if(ui->lineEdit_password->text() == "123456"){
        hide();
        emit onloginFinished(ui->lineEdit_password->text());
        delete ui;
//    }else{
//        hide();
//        emit onloginFinished(0);
//    }

}

void LoginDialog::process_Num(QString w){
    QString currentpass = ui->lineEdit_password->text();
    currentpass += w;
    ui->lineEdit_password->setText(currentpass);
}

void LoginDialog::on_pushButton_2_clicked()
{
    QString currentpass = ui->lineEdit_password->text();
    currentpass.remove(currentpass.length()-1,1);
    ui->lineEdit_password->setText(currentpass);
}

void LoginDialog::on_pushButton_clicked()
{
    ui->lineEdit_password->setText("");
}

void LoginDialog::on_pushButton_Cancel_clicked()
{
    hide();
    delete ui;
}
