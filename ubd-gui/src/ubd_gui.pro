include(../lib/qextserialport/src/qextserialport.pri)
include(../lib/log4qt-master/src/log4qt/log4qt.pri)
QT       += core gui
QT       += xml
QT       += network

greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = zdm-mf
TEMPLATE = app


SOURCES += main.cpp\
    keyboard.cpp \
    socketclient.cpp \
    logindialog.cpp

HEADERS  += keyboard.h \
    socketclient.h \
    logindialog.h

FORMS    += keyboard.ui \
    logindialog.ui
