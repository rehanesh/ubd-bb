#include "beeper.h"
#include "log4qt/logger.h"
#include <QFile>
#include <QByteArray>
#include <QAudio>
#include <QAudioFormat>
#include <QAudioDeviceInfo>

#define BEEP_WAV "../res/beep.wav"

Beeper::Beeper(QObject* parent) :
    QObject(parent)
{
    m_pAudioFile = new QFile(BEEP_WAV);
    if (m_pAudioFile->open(QIODevice::ReadOnly))
    {
         QAudioFormat format;
         format.setFrequency(44100);
         format.setChannels(1);
         format.setSampleSize(16);
         format.setCodec("audio/pcm");
         format.setByteOrder(QAudioFormat::LittleEndian);
         format.setSampleType(QAudioFormat::SignedInt);

         QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
         if (info.isFormatSupported(format))
         {
             logger()->debug("audio device name %1", info.deviceName());
             m_pAudioOutput = new QAudioOutput(info, format);
             m_pAudioOutput->setNotifyInterval(100);
             connect(m_pAudioOutput, SIGNAL(stateChanged(QAudio::State)),
                     SLOT(stop(QAudio::State)));
         }
         else
         {
             logger()->error("raw audio format not supported by backend.");
         }
    }
}

Beeper::~Beeper()
{
    if (m_pAudioOutput)
    {
        delete m_pAudioOutput;
        m_pAudioOutput = 0;
    }

    if (m_pAudioFile->isOpen())
    {
        m_pAudioFile->close();
    }
    delete m_pAudioFile;
    m_pAudioFile = 0;
}

void Beeper::play(int times)
{
    //logger()->debug("play() times %1", times);
    m_times = times;
    this->start();
}

void Beeper::start()
{
    //logger()->debug("start playing audio");
    if (m_pAudioFile->isOpen() && m_pAudioOutput)
    {
        m_pAudioFile->reset();
        m_pAudioOutput->start(m_pAudioFile);
    }
}

void Beeper::stop(QAudio::State state)
{
    //logger()->debug("stop playing audio, state %1", state);
    if (m_pAudioOutput && state == QAudio::IdleState)
    {
        m_pAudioOutput->stop();
        m_times --;
        if (m_times > 0)
        {
            this->start();
        }
    }
}
