#include "keyboard.h"
#include "zdm.h"
#include "log4qt/consoleappender.h"
#include "log4qt/logger.h"
#include "log4qt/ttcclayout.h"
#include "log4qt/logmanager.h"
#include <QApplication>
#include <QObject>
#include <QString>
#include <QByteArray>
#include <QtCore>
#include <log4qt/helpers/initialisationhelper.h>
#include "socketserver.h"


void initLogger()
{
    QSettings s;
    s.clear();

    // Set logging level for Log4Qt to TRACE
    s.beginGroup("Log4Qt");
    s.setValue("Configuration", "../conf/log4qt.properties");
    //    s.setValue("Debug", "TRACE");

//    // Configure logging to log to the file  using the level TRACE
//    s.beginGroup("Properties");
//    s.setValue("log4j.appender.A1", "org.apache.log4j.DailyRollingFileAppender");
//    s.setValue("log4j.appender.A1.file", "converterbox.log");
//    s.setValue("log4j.appender.A1.layout", "org.apache.log4j.TTCCLayout");
//    s.setValue("log4j.appender.A1.layout.DateFormat", "");
//    s.setValue("log4j.appender.A1.DatePattern", "'.'yyyy-MM-dd-a");
//    s.setValue("log4j.appender.A1.AppendFile", true);
//    s.setValue("log4j.appender.A1.MaxBackupIndex" , 5);
//    s.setValue("log4j.rootLogger", "TRACE, A1");
    s.endGroup();
//    Log4Qt::LogManager::configureLogLogger();

//    Log4Qt::InitialisationHelper::setting("DefaultInitOverride", "false");
//    Log4Qt::InitialisationHelper::setting("Configuration", "../conf/log4qt.properties");
//    Log4Qt::LogManager::startup();
    Log4Qt::Logger::logger("zdm-mf")->info("Starting...");
}

int main(int argc, char *argv[])
{
    QApplication app(argc, argv);
    initLogger();
    Zdm zdm;

    return app.exec();
}
