#ifndef CONVERTER_H
#define CONVERTER_H

#include <QObject>
#include <QByteArray>
#include <log4qt/logmanager.h>

class Converter : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Converter)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit Converter(QObject* parent = 0);
    ~Converter();

public slots:
    QByteArray convertHostMessage(const QByteArray& msg,
                                  unsigned char stx,
                                  unsigned char etx,
                                  int raddr,
                                  int packetId);
    QByteArray convertPowerbaseMessage(const QByteArray& msg);

private:
    void appendCheckSum(QByteArray& cs,
                        unsigned char stx,
                        unsigned char etx);
};

#endif // CONVERTER_H
