#include "repeater.h"

Repeater::Repeater(ConfigRepeater* pRepeater, QObject* parent):
        QObject(parent)
{
    m_addr = pRepeater->addr();
}

Repeater::~Repeater()
{
}

unsigned char Repeater::addr()
{
    return m_addr;
}
