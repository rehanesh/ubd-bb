#ifndef CONFIG_H
#define CONFIG_H

#include <QDomElement>
#include <QString>
#include <QtCore>
#include <log4qt/logger.h>

// device
class ConfigDevice : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigDevice)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigDevice(QDomElement e, QObject* parent = 0);
    ~ConfigDevice();
    QString type();
    QString port();
    QString Sockettype();
    QString openmode();
    QString baudrate();
    QString databits();
    QString parity();
    QString stopbits();
    QString flowcontrol();
    QString rtscontrol();
    long timeout();
    unsigned char stxchar();
    unsigned char etxchar();

private:
    int parseToNumber(const char* str);
    QString m_type;
    QString m_sockettype;
    QString m_port;
    QString m_openmode;
    QString m_baudrate;
    QString m_databits;
    QString m_parity;
    QString m_stopbits;
    QString m_flowcontrol;
    QString m_rtscontrol;
    long m_timeout;
    unsigned char m_stxchar;
    unsigned char m_etxchar;
};

// scanner
class ConfigScanner : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigScanner)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigScanner(QDomElement e, QObject* parent = 0);
    ~ConfigScanner();
    ConfigDevice* device();
private:
    ConfigDevice* m_pDevice;
};

// light
class ConfigLight : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigLight)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigLight(QDomElement e, QObject* parent = 0);
    ~ConfigLight();
    ConfigDevice* device();
private:
    ConfigDevice* m_pDevice;
};

// repeater
class ConfigRepeater : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigRepeater)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigRepeater(QDomElement e, QObject* parent = 0);
    ~ConfigRepeater();
    int addr();
private:
    int m_addr;
};

// powerbase
class ConfigPowerbase : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigPowerbase)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigPowerbase(QDomElement e, QObject* parent = 0);
    ~ConfigPowerbase();
    int addr();
    ConfigDevice* device();
    ConfigRepeater* repeater();
private:
    int m_addr;
    ConfigDevice* m_pDevice;
    ConfigRepeater* m_pRepeater;
};

// previoussiblingzdm, nextsiblingzdm
class ConfigSiblingZdm : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigSiblingZdm)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigSiblingZdm(QDomElement e, QObject* parent = 0);
    ~ConfigSiblingZdm();
    ConfigDevice* device();
private:
    ConfigDevice* m_pDevice;
};

// host
class ConfigHost : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigHost)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigHost(QDomElement e, QObject* parent = 0);
    ~ConfigHost();
    ConfigDevice* device();
private:
    ConfigDevice* m_pDevice;
};

// host RDS
class ConfigHostRDS : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigHostRDS)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigHostRDS(QDomElement e, QObject* parent = 0);
    ~ConfigHostRDS();
    ConfigDevice* device();
private:
    ConfigDevice* m_pDevice;
};


// hostGui
class ConfigHostGui : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigHostGui)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigHostGui(QDomElement e, QObject* parent = 0);
    ~ConfigHostGui();
    ConfigDevice* device();
private:
    ConfigDevice* m_pDevice;
};

// zdm
class ConfigZdm : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(ConfigZdm)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit ConfigZdm(QDomElement e, QObject* parent = 0);
    ~ConfigZdm();
    int addr();
    int rds();
    QString version();
    QString data_bus();
    QString virtual3dd();
    QString flip_layout();
    ConfigHost* host();
    ConfigHostRDS* hostRDS();
    ConfigHostGui* hostGui();
    ConfigSiblingZdm* previoussiblingzdm();
    ConfigSiblingZdm* nextsiblingzdm();
    ConfigPowerbase* powerbase();
    ConfigLight* light();
    ConfigScanner* scanner();
private:
    int m_addr;
    int m_rds;
    QString m_version;
    QString m_virtual3dd;
    QString m_data_bus;
    QString m_flip_layout;
    ConfigHost* m_pHost;
    ConfigHostRDS* m_pHostRDS;
    ConfigHostGui* m_pHostGui;
    ConfigSiblingZdm* m_pPreviousSiblingZdm;
    ConfigSiblingZdm* m_pNextSiblingZdm;
    ConfigPowerbase* m_pPowerbase;
    ConfigLight* m_pLight;
    ConfigScanner* m_pScanner;
};

// config
class Config : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Config)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    static Config* instance();
    ConfigZdm* zdm();
private:
    explicit Config(QObject* parent = 0);
    ~Config();
    static Config* m_pInstance;
    ConfigZdm* m_pZdm;
};

#endif // CONFIG_H
