#include "serialport.h"
#include <QTimer>

SerialPort::SerialPort(const QString& name,
                       QueryMode mode)
    : QextSerialPort(name, mode)
{
    QextSerialPort::parent();
    connect(this, SIGNAL(dsrChanged(bool)), this, SLOT(handleDsrChanged(bool)));
}

SerialPort::~SerialPort()
{
    if (this->isOpen())
    {
        this->flush();
        this->close();
    }
}

bool SerialPort::opens(QIODevice::OpenMode mode)
{
    if (!this->isOpen())
    {
        logger()->info("opening %1 ...", this->portName());
        if (this->open(mode))
        {
            if (!(this->lineStatus() & LS_DSR))
            {
                logger()->warn("device %1 is not turned on",
                               this->portName());
            }
            logger()->info("listening for data on %1",
                           this->portName());
            return true;
        }
        else
        {
            logger()->error("failed to open %1", this->portName());
            QTimer::singleShot(500, this, SLOT(opens()));
            return false;
        }
    }
    return true;
}

QByteArray SerialPort::data()
{
    QByteArray data;
    if (!this->isOpen())
    {
        if (!opens(this->openMode()))
        {
            return data;
        }
    }
    if (this->bytesAvailable() != 0)
    {
        data.resize(this->bytesAvailable());
        this->read(data.data(), data.size());
    }
    return data;
}

QIODevice::OpenMode SerialPort::serialOpenMode(QString openmode)
{
    openmode = openmode.toUpper();
    if (openmode == "R")
    {
        return QIODevice::ReadOnly|QIODevice::Unbuffered;
    }
    if (openmode == "W")
    {
        return QIODevice::WriteOnly|QIODevice::Unbuffered;
    }
    if (openmode == "RW")
    {
        return QIODevice::ReadWrite|QIODevice::Unbuffered;
    }
    return QIODevice::ReadOnly|QIODevice::Unbuffered;
}

BaudRateType SerialPort::serialBaudRate(QString baudrate)
{
    if (baudrate == "1200")
    {
        return BAUD1200;
    }
    if (baudrate == "2400")
    {
        return BAUD2400;
    }
    if (baudrate == "4800")
    {
        return BAUD4800;
    }
    if (baudrate == "9600")
    {
        return BAUD9600;
    }
    if (baudrate == "19200")
    {
        return BAUD19200;
    }
    if (baudrate == "115200")
    {
        return BAUD115200;
    }
    return BAUD19200;
}

DataBitsType SerialPort::serialDataBits(QString databits)
{
    if (databits == "5")
    {
        return DATA_5;
    }
    if (databits == "6")
    {
        return DATA_6;
    }
    if (databits == "7")
    {
        return DATA_7;
    }
    if (databits == "8")
    {
        return DATA_8;
    }
    return DATA_8;
}

ParityType SerialPort::serialParity(QString parity)
{
    parity = parity.toUpper();
    if (parity == "N")
    {
        return PAR_NONE;
    }
    if (parity == "O")
    {
        return PAR_ODD;
    }
    if (parity == "E")
    {
        return PAR_EVEN;
    }
    if (parity == "S")
    {
        return PAR_SPACE;
    }
    return PAR_NONE;
}

StopBitsType SerialPort::serialStopBits(QString stopbits){

    if (stopbits == "1")
    {
        return STOP_1;
    }
    if (stopbits == "2")
    {
        return STOP_2;
    }
    return STOP_1;
}

FlowType SerialPort::serialFlowControl(QString flowcontrol)
{
    flowcontrol = flowcontrol.toUpper();
    if (flowcontrol == "OFF")
    {
        return FLOW_OFF;
    }
    if (flowcontrol == "HARDWARE")
    {
        return FLOW_HARDWARE;
    }
    if (flowcontrol == "XONXOFF")
    {
        return FLOW_XONXOFF;
    }
    return FLOW_OFF;
}

void SerialPort::handleDsrChanged(bool status)
{
    if (status)
    {
        logger()->warn("Device was turned on");
    }
    else
    {
        logger()->warn("Device was turned off");
    }
}
