#ifndef SERIALPORT_H
#define SERIALPORT_H

#include "qextserialport.h"
#include <QString>
#include <QByteArray>
#include <log4qt/logmanager.h>

class SerialPort : public QextSerialPort
{
    Q_OBJECT
    Q_DISABLE_COPY(SerialPort)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    SerialPort(const QString& name,
               QueryMode mode=EventDriven);
    ~SerialPort();

    QByteArray data();
    static QIODevice::OpenMode serialOpenMode(QString openmode);
    static BaudRateType serialBaudRate(QString baudrate);
    static DataBitsType serialDataBits(QString databits);
    static ParityType serialParity(QString parity);
    static StopBitsType serialStopBits(QString stopbits);
    static FlowType serialFlowControl(QString flowcontrol);

public slots:
    bool opens(QIODevice::OpenMode mode);

private slots:
    void handleDsrChanged(bool status);
};

#endif // SERIALPORT_H
