#ifndef HostRDS_H
#define HostRDS_H

#include "config.h"
#include "device.h"
#include "gpio.h"
#include <QObject>
#include <QString>
#include <QByteArray>
#include <QTimer>
#include <deque>
#include <log4qt/logmanager.h>

class HostRDS : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(HostRDS)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit HostRDS(ConfigHostRDS* pHostRDS, QObject* parent = 0);
    ~HostRDS();

    QTimer* timer();
    QTimer* RTStimer();
    Device* device();
    void loadTXbuff(const QByteArray& msg, bool check);
    void checkRTS();
    std::deque <QByteArray> message_buff;
    bool waiting_OK;
    bool TXing;
//    QString RTSEnable;


signals:
    void GuimessageReceivedFromRDS(const QByteArray& msg);

public slots:
    void receive(const QByteArray& msg);
    void transmit(const QByteArray& msg);



private slots:
//    void receive(const QByteArray& msg);
//    void transmit();
//    void reTransmit();
//    void checkCTS();
//    void test();

private:
    QTimer* m_pTimer;
    QTimer* m_pRTSTimer;
    Device* m_pDevice;
    Gpio* m_pGpio;
    QByteArray m_message;
    int m_retries;
    int rts_retries;
};

#endif // HostRDS_H
