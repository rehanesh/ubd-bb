#ifndef DEVICE_H
#define DEVICE_H

#include "config.h"
#include "serialport.h"
#include "socketclient.h"
#include "socketserver.h"
#include <QObject>
#include <QByteArray>
#include <QIODevice>
#include <log4qt/logmanager.h>

class Device : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Device)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit Device(ConfigDevice* pDevice, QObject* parent = 0);
    ~Device();

    unsigned char stx();
    unsigned char etx();
    QString rtscontrol();
    void write(const QByteArray& data);
    void SendtoGui(const QByteArray& msg);

signals:
    void messageReceived(const QByteArray& msg);
    void SendToRDS(const QByteArray& msg);
    void GuimessageReceivedFromRDS(const QByteArray& msg);





private slots:
    void read();
    void readGuiMessages();
    void ReadMessagesFromGUI();
    void onNewConnection();
    void clientDisconnected();
    void checkTimer();


private:
    SerialPort* m_pSerial;
    SocketClient* m_pSocket;
    SocketServer* m_pServerSocket;
    QByteArray m_buffer;
    QString string_buf;
    unsigned char m_stx;
    unsigned char m_etx;
    QString m_rtscontrol;
    QTcpSocket *clientSocket;
    QTimer* mainTimer;
    bool reset_sent;

};

#endif // DEVICE_H
