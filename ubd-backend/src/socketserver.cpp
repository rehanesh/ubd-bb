#include "socketserver.h"

#include <QDebug>
#include <QHostAddress>
#include <QAbstractSocket>

SocketServer::SocketServer(QString port)
{
    m_port = port.toLong();
//    connect(this, SIGNAL(error(QAbstractSocket::SocketError)),
//            this, SLOT(handleSocketError(QAbstractSocket::SocketError)));
    _server = new QTcpServer(this);
    client_connected = false;

   // connect(_server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
}
void SocketServer::wait_for_connections(){
    logger()->debug("Listening on Port %1",m_port);
    _server->listen(QHostAddress::Any, m_port);
    return;
}

QTcpSocket* SocketServer::getClientSocket(){

  clientSocket = _server->nextPendingConnection();
  return clientSocket;
}

QTcpServer* SocketServer::getServer(){

  return _server;
}



//SocketServer::SocketServer(QObject *parent) : QObject(parent)

//{
//    _server = new QTcpServer(this);
//    logger()->debug("Listening on Port 10101");
//    _server->listen(QHostAddress::Any, 10101);
//    connect(_server, SIGNAL(newConnection()), this, SLOT(onNewConnection()));
//}

SocketServer::~SocketServer()
{

}
void SocketServer::setClientConnected(bool value){
client_connected = value;
}

#if 0
void SocketServer::onNewConnection()
{
   logger()->debug("Client Connected");

   clientSocket = _server->nextPendingConnection();
   logger()->debug("IP %1",clientSocket->peerAddress().toString());
   connect(clientSocket, SIGNAL(readyRead()), this, SLOT(onReadyRead()));
   connect(clientSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), this, SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));

    //_sockets.push_back(clientSocket);
//    for (QTcpSocket* socket : _sockets) {
//        socket->write(QByteArray::fromStdString(clientSocket->peerAddress().toString().toStdString() + " connected to server !\n"));
//    }
}
#endif
void SocketServer::onSocketStateChanged(QAbstractSocket::SocketState socketState)
{
    logger()->debug("Socket state Changed");
    if (socketState == QAbstractSocket::UnconnectedState)
    {
        QTcpSocket* sender = static_cast<QTcpSocket*>(QObject::sender());
        //_sockets.removeOne(sender);
    }
}

QByteArray SocketServer::read()
{
     QByteArray data;
     if (clientSocket->bytesAvailable() != 0)
     {
          data.resize(clientSocket->bytesAvailable());
          clientSocket->read(data.data(), data.size());
     }
     //logger()->debug("Data Read %1",QString(data));
      return data;
}

void SocketServer::write(const QByteArray& msg)
{
    if(client_connected)
    clientSocket->write(msg);
    else
    logger()->debug("GUI Client not connected yet");



}
