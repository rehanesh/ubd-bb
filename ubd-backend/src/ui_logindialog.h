/********************************************************************************
** Form generated from reading UI file 'logindialog.ui'
**
** Created: Tue Oct 23 02:38:19 2018
**      by: Qt User Interface Compiler version 4.8.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_LOGINDIALOG_H
#define UI_LOGINDIALOG_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QDialog>
#include <QtGui/QGridLayout>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QLineEdit>
#include <QtGui/QPushButton>
#include <QtGui/QSplitter>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_LoginDialog
{
public:
    QWidget *layoutWidget_2;
    QGridLayout *gridLayout;
    QPushButton *panelButton_star;
    QPushButton *panelButton_9;
    QPushButton *panelButton_8;
    QPushButton *panelButton_7;
    QPushButton *panelButton_6;
    QPushButton *panelButton_1;
    QPushButton *panelButton_5;
    QPushButton *panelButton_0;
    QPushButton *panelButton_4;
    QPushButton *panelButton_3;
    QPushButton *panelButton_2;
    QPushButton *panelButton_hash;
    QPushButton *pushButton;
    QPushButton *pushButton_2;
    QLineEdit *lineEdit_password;
    QLabel *label;
    QSplitter *splitter;
    QPushButton *pushButton_Login;
    QPushButton *pushButton_Cancel;

    void setupUi(QDialog *LoginDialog)
    {
        if (LoginDialog->objectName().isEmpty())
            LoginDialog->setObjectName(QString::fromUtf8("LoginDialog"));
        LoginDialog->resize(332, 390);
        layoutWidget_2 = new QWidget(LoginDialog);
        layoutWidget_2->setObjectName(QString::fromUtf8("layoutWidget_2"));
        layoutWidget_2->setGeometry(QRect(20, 140, 295, 218));
        gridLayout = new QGridLayout(layoutWidget_2);
        gridLayout->setObjectName(QString::fromUtf8("gridLayout"));
        gridLayout->setContentsMargins(0, 0, 0, 0);
        panelButton_star = new QPushButton(layoutWidget_2);
        panelButton_star->setObjectName(QString::fromUtf8("panelButton_star"));
        QSizePolicy sizePolicy(QSizePolicy::Minimum, QSizePolicy::Minimum);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(panelButton_star->sizePolicy().hasHeightForWidth());
        panelButton_star->setSizePolicy(sizePolicy);
        panelButton_star->setMinimumSize(QSize(45, 40));
        panelButton_star->setFocusPolicy(Qt::NoFocus);
        panelButton_star->setProperty("buttonValue", QVariant(QChar(42)));

        gridLayout->addWidget(panelButton_star, 3, 0, 1, 1);

        panelButton_9 = new QPushButton(layoutWidget_2);
        panelButton_9->setObjectName(QString::fromUtf8("panelButton_9"));
        sizePolicy.setHeightForWidth(panelButton_9->sizePolicy().hasHeightForWidth());
        panelButton_9->setSizePolicy(sizePolicy);
        panelButton_9->setMinimumSize(QSize(45, 40));
        panelButton_9->setFocusPolicy(Qt::NoFocus);
        panelButton_9->setProperty("buttonValue", QVariant(QChar(57)));

        gridLayout->addWidget(panelButton_9, 2, 2, 1, 1);

        panelButton_8 = new QPushButton(layoutWidget_2);
        panelButton_8->setObjectName(QString::fromUtf8("panelButton_8"));
        sizePolicy.setHeightForWidth(panelButton_8->sizePolicy().hasHeightForWidth());
        panelButton_8->setSizePolicy(sizePolicy);
        panelButton_8->setMinimumSize(QSize(45, 40));
        panelButton_8->setFocusPolicy(Qt::NoFocus);
        panelButton_8->setProperty("buttonValue", QVariant(QChar(56)));

        gridLayout->addWidget(panelButton_8, 2, 1, 1, 1);

        panelButton_7 = new QPushButton(layoutWidget_2);
        panelButton_7->setObjectName(QString::fromUtf8("panelButton_7"));
        sizePolicy.setHeightForWidth(panelButton_7->sizePolicy().hasHeightForWidth());
        panelButton_7->setSizePolicy(sizePolicy);
        panelButton_7->setMinimumSize(QSize(45, 40));
        panelButton_7->setFocusPolicy(Qt::NoFocus);
        panelButton_7->setProperty("buttonValue", QVariant(QChar(55)));

        gridLayout->addWidget(panelButton_7, 2, 0, 1, 1);

        panelButton_6 = new QPushButton(layoutWidget_2);
        panelButton_6->setObjectName(QString::fromUtf8("panelButton_6"));
        sizePolicy.setHeightForWidth(panelButton_6->sizePolicy().hasHeightForWidth());
        panelButton_6->setSizePolicy(sizePolicy);
        panelButton_6->setMinimumSize(QSize(45, 40));
        panelButton_6->setFocusPolicy(Qt::NoFocus);
        panelButton_6->setProperty("buttonValue", QVariant(QChar(54)));

        gridLayout->addWidget(panelButton_6, 1, 2, 1, 1);

        panelButton_1 = new QPushButton(layoutWidget_2);
        panelButton_1->setObjectName(QString::fromUtf8("panelButton_1"));
        sizePolicy.setHeightForWidth(panelButton_1->sizePolicy().hasHeightForWidth());
        panelButton_1->setSizePolicy(sizePolicy);
        panelButton_1->setMinimumSize(QSize(45, 40));
        panelButton_1->setFocusPolicy(Qt::NoFocus);
        panelButton_1->setProperty("buttonValue", QVariant(QChar(49)));

        gridLayout->addWidget(panelButton_1, 0, 0, 1, 1);

        panelButton_5 = new QPushButton(layoutWidget_2);
        panelButton_5->setObjectName(QString::fromUtf8("panelButton_5"));
        sizePolicy.setHeightForWidth(panelButton_5->sizePolicy().hasHeightForWidth());
        panelButton_5->setSizePolicy(sizePolicy);
        panelButton_5->setMinimumSize(QSize(45, 40));
        panelButton_5->setFocusPolicy(Qt::NoFocus);
        panelButton_5->setProperty("buttonValue", QVariant(QChar(53)));

        gridLayout->addWidget(panelButton_5, 1, 1, 1, 1);

        panelButton_0 = new QPushButton(layoutWidget_2);
        panelButton_0->setObjectName(QString::fromUtf8("panelButton_0"));
        sizePolicy.setHeightForWidth(panelButton_0->sizePolicy().hasHeightForWidth());
        panelButton_0->setSizePolicy(sizePolicy);
        panelButton_0->setMinimumSize(QSize(45, 40));
        panelButton_0->setFocusPolicy(Qt::NoFocus);
        panelButton_0->setProperty("buttonValue", QVariant(QChar(48)));

        gridLayout->addWidget(panelButton_0, 3, 1, 1, 1);

        panelButton_4 = new QPushButton(layoutWidget_2);
        panelButton_4->setObjectName(QString::fromUtf8("panelButton_4"));
        sizePolicy.setHeightForWidth(panelButton_4->sizePolicy().hasHeightForWidth());
        panelButton_4->setSizePolicy(sizePolicy);
        panelButton_4->setMinimumSize(QSize(45, 40));
        panelButton_4->setFocusPolicy(Qt::NoFocus);
        panelButton_4->setProperty("buttonValue", QVariant(QChar(52)));

        gridLayout->addWidget(panelButton_4, 1, 0, 1, 1);

        panelButton_3 = new QPushButton(layoutWidget_2);
        panelButton_3->setObjectName(QString::fromUtf8("panelButton_3"));
        sizePolicy.setHeightForWidth(panelButton_3->sizePolicy().hasHeightForWidth());
        panelButton_3->setSizePolicy(sizePolicy);
        panelButton_3->setMinimumSize(QSize(45, 40));
        panelButton_3->setFocusPolicy(Qt::NoFocus);
        panelButton_3->setProperty("buttonValue", QVariant(QChar(51)));

        gridLayout->addWidget(panelButton_3, 0, 2, 1, 1);

        panelButton_2 = new QPushButton(layoutWidget_2);
        panelButton_2->setObjectName(QString::fromUtf8("panelButton_2"));
        sizePolicy.setHeightForWidth(panelButton_2->sizePolicy().hasHeightForWidth());
        panelButton_2->setSizePolicy(sizePolicy);
        panelButton_2->setMinimumSize(QSize(45, 40));
        panelButton_2->setFocusPolicy(Qt::NoFocus);
        panelButton_2->setProperty("buttonValue", QVariant(QChar(50)));

        gridLayout->addWidget(panelButton_2, 0, 1, 1, 1);

        panelButton_hash = new QPushButton(layoutWidget_2);
        panelButton_hash->setObjectName(QString::fromUtf8("panelButton_hash"));
        sizePolicy.setHeightForWidth(panelButton_hash->sizePolicy().hasHeightForWidth());
        panelButton_hash->setSizePolicy(sizePolicy);
        panelButton_hash->setMinimumSize(QSize(45, 40));
        panelButton_hash->setFocusPolicy(Qt::NoFocus);
        panelButton_hash->setProperty("buttonValue", QVariant(QChar(35)));

        gridLayout->addWidget(panelButton_hash, 3, 2, 1, 1);

        pushButton = new QPushButton(layoutWidget_2);
        pushButton->setObjectName(QString::fromUtf8("pushButton"));

        gridLayout->addWidget(pushButton, 4, 0, 1, 1);

        pushButton_2 = new QPushButton(layoutWidget_2);
        pushButton_2->setObjectName(QString::fromUtf8("pushButton_2"));

        gridLayout->addWidget(pushButton_2, 4, 2, 1, 1);

        lineEdit_password = new QLineEdit(LoginDialog);
        lineEdit_password->setObjectName(QString::fromUtf8("lineEdit_password"));
        lineEdit_password->setGeometry(QRect(101, 51, 137, 22));
        label = new QLabel(LoginDialog);
        label->setObjectName(QString::fromUtf8("label"));
        label->setGeometry(QRect(120, 20, 100, 16));
        splitter = new QSplitter(LoginDialog);
        splitter->setObjectName(QString::fromUtf8("splitter"));
        splitter->setGeometry(QRect(70, 90, 186, 40));
        splitter->setOrientation(Qt::Horizontal);
        pushButton_Login = new QPushButton(splitter);
        pushButton_Login->setObjectName(QString::fromUtf8("pushButton_Login"));
        splitter->addWidget(pushButton_Login);
        pushButton_Cancel = new QPushButton(splitter);
        pushButton_Cancel->setObjectName(QString::fromUtf8("pushButton_Cancel"));
        splitter->addWidget(pushButton_Cancel);

        retranslateUi(LoginDialog);

        QMetaObject::connectSlotsByName(LoginDialog);
    } // setupUi

    void retranslateUi(QDialog *LoginDialog)
    {
        LoginDialog->setWindowTitle(QString());
        panelButton_star->setText(QApplication::translate("LoginDialog", "*", 0, QApplication::UnicodeUTF8));
        panelButton_9->setText(QApplication::translate("LoginDialog", "9", 0, QApplication::UnicodeUTF8));
        panelButton_8->setText(QApplication::translate("LoginDialog", "8", 0, QApplication::UnicodeUTF8));
        panelButton_7->setText(QApplication::translate("LoginDialog", "7", 0, QApplication::UnicodeUTF8));
        panelButton_6->setText(QApplication::translate("LoginDialog", "6", 0, QApplication::UnicodeUTF8));
        panelButton_1->setText(QApplication::translate("LoginDialog", "1", 0, QApplication::UnicodeUTF8));
        panelButton_5->setText(QApplication::translate("LoginDialog", "5", 0, QApplication::UnicodeUTF8));
        panelButton_0->setText(QApplication::translate("LoginDialog", "0", 0, QApplication::UnicodeUTF8));
        panelButton_4->setText(QApplication::translate("LoginDialog", "4", 0, QApplication::UnicodeUTF8));
        panelButton_3->setText(QApplication::translate("LoginDialog", "3", 0, QApplication::UnicodeUTF8));
        panelButton_2->setText(QApplication::translate("LoginDialog", "2", 0, QApplication::UnicodeUTF8));
        panelButton_hash->setText(QApplication::translate("LoginDialog", "#", 0, QApplication::UnicodeUTF8));
        pushButton->setText(QApplication::translate("LoginDialog", "Clear", 0, QApplication::UnicodeUTF8));
        pushButton_2->setText(QApplication::translate("LoginDialog", "Del", 0, QApplication::UnicodeUTF8));
        label->setText(QApplication::translate("LoginDialog", "Enter Pass Code", 0, QApplication::UnicodeUTF8));
        pushButton_Login->setText(QApplication::translate("LoginDialog", "OK", 0, QApplication::UnicodeUTF8));
        pushButton_Cancel->setText(QApplication::translate("LoginDialog", "Cancel", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class LoginDialog: public Ui_LoginDialog {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_LOGINDIALOG_H
