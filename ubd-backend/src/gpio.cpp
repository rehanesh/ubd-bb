#include "gpio.h"
#include <QFile>
#include <QTextStream>

#define GREEN "/sys/class/gpio/gpio9/"
#define RED "/sys/class/gpio/gpio171/"

Gpio::Gpio(QObject* parent) :
    QObject(parent)
{
//    QString path = "/sys/class/gpio/gpio93/";
//	QString value = "1";
//    logger()->debug("active low %1, value %2", path, value);
//    QFile dfile(path + "active_low");
//    if (!dfile.open(QIODevice::ReadWrite))
//    {
//        logger()->error("unable to open file %1", dfile.fileName());
//        return;
//    }
//    if (dfile.write("1") == -1)
//    {
//         logger()->error("write active low failed");
//         dfile.close();
//         return;
//    }
//    dfile.close();
}
Gpio::~Gpio()
{
}

void Gpio::noneon()
{
   this->turnon(RED);
   logger()->debug("red beacon off");
   this->turnon(GREEN);
   logger()->debug("green beacon off");
}

void Gpio::greenon()
{
   this->turnoff(RED);
   logger()->debug("red beacon off");
   this->turnon(GREEN);
   logger()->debug("green beacon on");
}

void Gpio::redon()
{

    this->turnoff(GREEN);
    logger()->debug("green beacon off");
    this->turnon(RED);
    logger()->debug("red beacon on");
}

void Gpio::turnon(QString path)
{
    this->output(path, "1");
}

void Gpio::turnoff(QString path)
{
    this->output(path, "0");
}

void Gpio::output(QString path, QByteArray value)
{
    logger()->debug("path %1, value %2", path, QString(value));
    QFile dfile(path + "direction");
    QFile vfile(path + "value");
    if (!dfile.open(QIODevice::ReadWrite))
    {
        logger()->error("unable to open file %1", dfile.fileName());
        return;
    }
    if (!vfile.open(QIODevice::ReadWrite))
    {
        logger()->error("unable to open file %1",  vfile.fileName());
        dfile.close();
        return;
    }
    if (dfile.write("out") == -1)
    {
         logger()->error("write direction failed");
         dfile.close();
         vfile.close();
         return;
    }
    if (vfile.write(value)  == -1)
    {
         logger()->error("write value failed");
    }
    dfile.close();
    vfile.close();
}

int Gpio::input(QString path)
{

    QString value;
    QFile vfile(path + "value");
    if (!vfile.open(QIODevice::ReadOnly))
    {
        logger()->error("unable to open file %1",  vfile.fileName());
        return -1;
    }
    value = vfile.readAll();
    vfile.close();
    logger()->debug("value read  %1",value);
     int val = value.toInt();
     logger()->debug("val %1",val);
     return val;
   // vfile.close();
    //bool ok;
    //int val = value.toInt(&ok, 10);
    //logger()->debug("conversion %1, value %2", ok, val);

  //  return 1;
}
