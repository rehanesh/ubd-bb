#include "host.h"
#include <QTimer>

#define RTS "/sys/class/gpio/gpio93/" //output
#define CTS "/sys/class/gpio/gpio57/" //input

Host::Host(ConfigHost* pHost, QObject* parent):
    QObject(parent)
{
    m_pTimer = new QTimer(this);
    m_pTimer->setInterval(1000);
    m_pRTSTimer = new QTimer(this);
    m_pRTSTimer->setInterval(20);
    m_pGpio = new Gpio();
    waiting_OK = false;
    TXing = false;

    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(reTransmit()));
    connect(m_pRTSTimer, SIGNAL(timeout()), this, SLOT(checkCTS()));

    m_pDevice = new Device(pHost->device());
    connect(m_pDevice, SIGNAL(messageReceived(QByteArray)),
            this, SLOT(receive(QByteArray)) );
}

Host::~Host()
{
    delete m_pTimer;
    m_pTimer = 0;

    delete m_pRTSTimer;
    m_pRTSTimer = 0;

    delete m_pDevice;
    m_pDevice = 0;

    delete m_pGpio;
    m_pGpio = 0;

}

QTimer* Host::timer()
{
    return m_pTimer;
}

QTimer* Host::RTStimer()
{
    return m_pRTSTimer;
}

Device* Host::device()
{
    return m_pDevice;
}

void Host::loadTXbuff(const QByteArray &msg, bool check){
    QByteArray temp = msg;
    int x = temp.indexOf(0xfd,1);
    if((x > 1 && temp.at(x+1) == 'O' && temp.at(x+2) == 'K' || temp.at(x+1) == 'A' && temp.at(x+2) == 'R') || (!check) /*msg from sibling*/) {
        temp.append("1");// No retry
    }else{
        int x = temp.indexOf(0xf9,1);
        //check for f9 and set retry last flag
        if(x > 0){
            temp.append("2");// wait for OK and retry
        }else{
            temp.append("1");
        }
    }
    message_buff.push_back(temp);

//    message_buff.append(temp);
//    qDebug() << "Host loadtxbuff... " << temp.toHex();
    qDebug() << "Host message buffer... " << message_buff.back().toHex();

        logger()->info("Rehanesh waiting_ok %1", waiting_OK);
    if (!waiting_OK)
        checkRTS();
}

void Host::checkRTS(){
//    RTSEnable = m_pDevice->rtscontrol();
    if(m_pDevice->rtscontrol() == "on"){
    // Set RTS and timer to check for CTS
        TXing = true;
        m_pGpio->output(RTS, "1");
        m_pRTSTimer->start();
        rts_retries = 0;
    }else{
        transmit();
    }
}

void Host::checkCTS()// off 20 mS timer
{
    if(rts_retries >= 3){
//        m_pRTSTimer->stop();
        rts_retries = 0;
        emit errorReceived("RTS / CTS to Host Failed");
    }else{
        rts_retries++;
        int input_value = m_pGpio->input(CTS);
        logger()->info("Gpio Input path:%1, value:%2", CTS, input_value);
        if (input_value == 1){
            m_pRTSTimer->stop();
            rts_retries = 0;
            transmit();
        }
    }
}

void Host::transmit()
{
    QByteArray temp;
    temp.clear();
    temp.append(message_buff.front());
    qDebug() << "Host temp buffer... " << temp.toHex();
    QString message_type = QString(temp.at(temp.length()-1));
    temp = temp.mid(0,temp.length()-1);//remove last byte of message
    logger()->info("SENDING to HOST hex:%1, ascii:%2, type:%3", QString(temp.toHex()), QString(temp), message_type);
    m_pDevice->write(temp);

    if (message_type == "1"){// no retries
        waiting_OK = false;
        message_buff.pop_front();
    }else if(message_type == "2"){
        waiting_OK = true;
        temp.append("3");
        message_buff.pop_front();
        message_buff.push_front(temp);
        m_pTimer->start();
        m_retries = 0;
    }

    m_pGpio->output(RTS, "0");// may need to check if Transmitting has finished
    TXing = false;
}

void Host::reTransmit()// off 1000 mS timer
{
    if (m_retries >= 3)
    {
        logger()->error("FAILED TX to HOST");
        emit errorReceived("Failed to Transmit to Host");
        m_retries = 0;
    }
    else
    {
        logger()->error("RESENDING to HOST");
        m_retries ++;
    }
    checkRTS();
}

void Host::receive(const QByteArray& msg)
{
    logger()->info("RECEIVED FROM HOST hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));
    emit messageReceived(msg);
}

// *********************************************
// *************** Testing only ****************
// *********************************************
void Host::test()
{
    // Beeper, F2
//    unsigned char mydataa[] = {
//       0xff, 0x80, 0xfd, 0x42, 0x33, 0xf2, 0x20, 0x50, 0x49, 0x43, 0x4b, 0x49, 0x4e, 0x47, 0x20, 0x32, 0x35, 0x37, 0x38, 0x33, 0x39, 0xfe
//    };
//    QByteArray a = QByteArray::fromRawData((char*)mydataa, sizeof(mydataa));
//    receive(a);
    // GPIO
//    unsigned char mydatab[] = {
//       0xff, 0x80, 0xf8, 0x10, 0xfe
//    };
//    QByteArray b = QByteArray::fromRawData((char*)mydatab, sizeof(mydatab));
//    receive(b);
    // Light
//    unsigned char mydatac[] = {
//        0xff, 0x80, 0xf4, 0x01, 0xf1, 0x55, 0x30, 0x31, 0xfb, 0x01, 0xfd, 0x41, 0x4c, 0xfd, 0x4f, 0x46, 0xf5, 0xf9, 0x17, 0xfe
//    };
//    QByteArray c = QByteArray::fromRawData((char*)mydatac, sizeof(mydatac));
//    receive(c);
}
// *********************************************
// *********************************************
// *********************************************
