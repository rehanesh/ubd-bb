#include "siblingzdm.h"

SiblingZdm::SiblingZdm(ConfigSiblingZdm* pSiblingZdm, QObject* parent) :
    QObject(parent)
{
    m_pDevice = new Device(pSiblingZdm->device());
    connect(m_pDevice, SIGNAL(messageReceived(QByteArray)),
            this, SLOT(receive(QByteArray)));
}

SiblingZdm::~SiblingZdm()
{
    delete m_pDevice;
    m_pDevice = 0;
}

Device* SiblingZdm::device()
{
    return m_pDevice;
}

void SiblingZdm::transmit(const QByteArray& msg)
{
    logger()->info("SENDING TO SIBLING ZDM hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));
    m_pDevice->write(msg);
}

void SiblingZdm::receive(const QByteArray& msg)
{
    logger()->info("RECEIVED FROM SIBLING ZDM hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));

    emit messageReceived(msg);
}
