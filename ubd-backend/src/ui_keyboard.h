/********************************************************************************
** Form generated from reading UI file 'keyboard.ui'
**
** Created: Tue Oct 23 02:38:19 2018
**      by: Qt User Interface Compiler version 4.8.2
**
** WARNING! All changes made in this file will be lost when recompiling UI file!
********************************************************************************/

#ifndef UI_KEYBOARD_H
#define UI_KEYBOARD_H

#include <QtCore/QVariant>
#include <QtGui/QAction>
#include <QtGui/QApplication>
#include <QtGui/QButtonGroup>
#include <QtGui/QHeaderView>
#include <QtGui/QLabel>
#include <QtGui/QMainWindow>
#include <QtGui/QMenu>
#include <QtGui/QMenuBar>
#include <QtGui/QPushButton>
#include <QtGui/QWidget>

QT_BEGIN_NAMESPACE

class Ui_Keyboard
{
public:
    QAction *action_quit;
    QAction *actionEnable_Maintenance_Mode;
    QWidget *widget;
    QPushButton *button_cancel;
    QLabel *label_cancel;
    QLabel *label_suspend;
    QPushButton *button_1;
    QPushButton *button_2;
    QLabel *label_link;
    QPushButton *button_3;
    QLabel *label_unlink;
    QPushButton *button_4;
    QLabel *label_test;
    QPushButton *button_5;
    QLabel *label_enter;
    QLabel *label_start;
    QPushButton *button_enter;
    QPushButton *button_numerals;
    QLabel *label_numerals;
    QPushButton *button_6;
    QLabel *label_next;
    QPushButton *button_7;
    QLabel *label_unload;
    QPushButton *button_8;
    QPushButton *button_9;
    QPushButton *button_0;
    QLabel *label_relight;
    QLabel *label_reprn;
    QLabel *label_product;
    QLabel *label_displaymessage;
    QLabel *label_dexion;
    QLabel *label_displayerror;
    QMenuBar *menuBar;
    QMenu *menuFile;

    void setupUi(QMainWindow *Keyboard)
    {
        if (Keyboard->objectName().isEmpty())
            Keyboard->setObjectName(QString::fromUtf8("Keyboard"));
        Keyboard->resize(800, 480);
        Keyboard->setAutoFillBackground(false);
        action_quit = new QAction(Keyboard);
        action_quit->setObjectName(QString::fromUtf8("action_quit"));
        actionEnable_Maintenance_Mode = new QAction(Keyboard);
        actionEnable_Maintenance_Mode->setObjectName(QString::fromUtf8("actionEnable_Maintenance_Mode"));
        widget = new QWidget(Keyboard);
        widget->setObjectName(QString::fromUtf8("widget"));
        button_cancel = new QPushButton(widget);
        button_cancel->setObjectName(QString::fromUtf8("button_cancel"));
        button_cancel->setEnabled(false);
        button_cancel->setGeometry(QRect(60, 290, 60, 60));
        QPalette palette;
        QBrush brush(QColor(0, 0, 0, 255));
        brush.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::WindowText, brush);
        QBrush brush1(QColor(255, 255, 255, 255));
        brush1.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Button, brush1);
        palette.setBrush(QPalette::Active, QPalette::Light, brush1);
        palette.setBrush(QPalette::Active, QPalette::Midlight, brush1);
        QBrush brush2(QColor(127, 127, 127, 255));
        brush2.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Dark, brush2);
        QBrush brush3(QColor(170, 170, 170, 255));
        brush3.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::Mid, brush3);
        palette.setBrush(QPalette::Active, QPalette::Text, brush);
        palette.setBrush(QPalette::Active, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Active, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Active, QPalette::Base, brush1);
        palette.setBrush(QPalette::Active, QPalette::Window, brush1);
        palette.setBrush(QPalette::Active, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Active, QPalette::AlternateBase, brush1);
        QBrush brush4(QColor(255, 255, 220, 255));
        brush4.setStyle(Qt::SolidPattern);
        palette.setBrush(QPalette::Active, QPalette::ToolTipBase, brush4);
        palette.setBrush(QPalette::Active, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::WindowText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Button, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Light, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Dark, brush2);
        palette.setBrush(QPalette::Inactive, QPalette::Mid, brush3);
        palette.setBrush(QPalette::Inactive, QPalette::Text, brush);
        palette.setBrush(QPalette::Inactive, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::ButtonText, brush);
        palette.setBrush(QPalette::Inactive, QPalette::Base, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Window, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Inactive, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipBase, brush4);
        palette.setBrush(QPalette::Inactive, QPalette::ToolTipText, brush);
        palette.setBrush(QPalette::Disabled, QPalette::WindowText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Button, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Light, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Midlight, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Dark, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Mid, brush3);
        palette.setBrush(QPalette::Disabled, QPalette::Text, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::BrightText, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ButtonText, brush2);
        palette.setBrush(QPalette::Disabled, QPalette::Base, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Window, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::Shadow, brush);
        palette.setBrush(QPalette::Disabled, QPalette::AlternateBase, brush1);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipBase, brush4);
        palette.setBrush(QPalette::Disabled, QPalette::ToolTipText, brush);
        button_cancel->setPalette(palette);
        button_cancel->setAutoFillBackground(true);
        label_cancel = new QLabel(widget);
        label_cancel->setObjectName(QString::fromUtf8("label_cancel"));
        label_cancel->setGeometry(QRect(45, 270, 90, 20));
        QFont font;
        font.setBold(true);
        font.setWeight(75);
        label_cancel->setFont(font);
        label_cancel->setAlignment(Qt::AlignCenter);
        label_suspend = new QLabel(widget);
        label_suspend->setObjectName(QString::fromUtf8("label_suspend"));
        label_suspend->setGeometry(QRect(225, 270, 90, 20));
        label_suspend->setFont(font);
        label_suspend->setAlignment(Qt::AlignCenter);
        button_1 = new QPushButton(widget);
        button_1->setObjectName(QString::fromUtf8("button_1"));
        button_1->setEnabled(false);
        button_1->setGeometry(QRect(150, 290, 60, 60));
        button_1->setFont(font);
        button_2 = new QPushButton(widget);
        button_2->setObjectName(QString::fromUtf8("button_2"));
        button_2->setEnabled(false);
        button_2->setGeometry(QRect(240, 290, 60, 60));
        button_2->setFont(font);
        label_link = new QLabel(widget);
        label_link->setObjectName(QString::fromUtf8("label_link"));
        label_link->setGeometry(QRect(315, 270, 90, 20));
        label_link->setFont(font);
        label_link->setAlignment(Qt::AlignCenter);
        button_3 = new QPushButton(widget);
        button_3->setObjectName(QString::fromUtf8("button_3"));
        button_3->setEnabled(false);
        button_3->setGeometry(QRect(330, 290, 60, 60));
        button_3->setFont(font);
        label_unlink = new QLabel(widget);
        label_unlink->setObjectName(QString::fromUtf8("label_unlink"));
        label_unlink->setGeometry(QRect(405, 270, 90, 20));
        label_unlink->setFont(font);
        label_unlink->setAlignment(Qt::AlignCenter);
        button_4 = new QPushButton(widget);
        button_4->setObjectName(QString::fromUtf8("button_4"));
        button_4->setEnabled(false);
        button_4->setGeometry(QRect(420, 290, 60, 60));
        button_4->setFont(font);
        label_test = new QLabel(widget);
        label_test->setObjectName(QString::fromUtf8("label_test"));
        label_test->setGeometry(QRect(495, 270, 90, 20));
        label_test->setFont(font);
        label_test->setAlignment(Qt::AlignCenter);
        button_5 = new QPushButton(widget);
        button_5->setObjectName(QString::fromUtf8("button_5"));
        button_5->setEnabled(false);
        button_5->setGeometry(QRect(510, 290, 60, 60));
        button_5->setFont(font);
        label_enter = new QLabel(widget);
        label_enter->setObjectName(QString::fromUtf8("label_enter"));
        label_enter->setGeometry(QRect(665, 270, 90, 20));
        label_enter->setFont(font);
        label_enter->setAlignment(Qt::AlignCenter);
        label_start = new QLabel(widget);
        label_start->setObjectName(QString::fromUtf8("label_start"));
        label_start->setGeometry(QRect(135, 270, 90, 20));
        label_start->setFont(font);
        label_start->setAlignment(Qt::AlignCenter);
        button_enter = new QPushButton(widget);
        button_enter->setObjectName(QString::fromUtf8("button_enter"));
        button_enter->setEnabled(false);
        button_enter->setGeometry(QRect(680, 290, 60, 60));
        button_numerals = new QPushButton(widget);
        button_numerals->setObjectName(QString::fromUtf8("button_numerals"));
        button_numerals->setEnabled(false);
        button_numerals->setGeometry(QRect(60, 380, 60, 60));
        label_numerals = new QLabel(widget);
        label_numerals->setObjectName(QString::fromUtf8("label_numerals"));
        label_numerals->setGeometry(QRect(45, 360, 90, 20));
        label_numerals->setFont(font);
        label_numerals->setAlignment(Qt::AlignCenter);
        button_6 = new QPushButton(widget);
        button_6->setObjectName(QString::fromUtf8("button_6"));
        button_6->setEnabled(false);
        button_6->setGeometry(QRect(150, 380, 60, 60));
        button_6->setFont(font);
        label_next = new QLabel(widget);
        label_next->setObjectName(QString::fromUtf8("label_next"));
        label_next->setGeometry(QRect(135, 360, 90, 20));
        label_next->setFont(font);
        label_next->setAlignment(Qt::AlignCenter);
        button_7 = new QPushButton(widget);
        button_7->setObjectName(QString::fromUtf8("button_7"));
        button_7->setEnabled(false);
        button_7->setGeometry(QRect(240, 380, 60, 60));
        button_7->setFont(font);
        label_unload = new QLabel(widget);
        label_unload->setObjectName(QString::fromUtf8("label_unload"));
        label_unload->setGeometry(QRect(225, 360, 90, 20));
        label_unload->setFont(font);
        label_unload->setAlignment(Qt::AlignCenter);
        button_8 = new QPushButton(widget);
        button_8->setObjectName(QString::fromUtf8("button_8"));
        button_8->setEnabled(false);
        button_8->setGeometry(QRect(330, 380, 60, 60));
        button_8->setFont(font);
        button_9 = new QPushButton(widget);
        button_9->setObjectName(QString::fromUtf8("button_9"));
        button_9->setEnabled(false);
        button_9->setGeometry(QRect(420, 380, 60, 60));
        button_9->setFont(font);
        button_0 = new QPushButton(widget);
        button_0->setObjectName(QString::fromUtf8("button_0"));
        button_0->setEnabled(false);
        button_0->setGeometry(QRect(510, 380, 60, 60));
        button_0->setFont(font);
        label_relight = new QLabel(widget);
        label_relight->setObjectName(QString::fromUtf8("label_relight"));
        label_relight->setGeometry(QRect(315, 360, 90, 20));
        label_relight->setFont(font);
        label_relight->setAlignment(Qt::AlignCenter);
        label_reprn = new QLabel(widget);
        label_reprn->setObjectName(QString::fromUtf8("label_reprn"));
        label_reprn->setGeometry(QRect(405, 360, 90, 20));
        label_reprn->setFont(font);
        label_reprn->setAlignment(Qt::AlignCenter);
        label_product = new QLabel(widget);
        label_product->setObjectName(QString::fromUtf8("label_product"));
        label_product->setGeometry(QRect(495, 360, 90, 20));
        label_product->setFont(font);
        label_product->setAlignment(Qt::AlignCenter);
        label_displaymessage = new QLabel(widget);
        label_displaymessage->setObjectName(QString::fromUtf8("label_displaymessage"));
        label_displaymessage->setGeometry(QRect(90, 200, 620, 40));
        QFont font1;
        font1.setPointSize(16);
        font1.setBold(true);
        font1.setWeight(75);
        label_displaymessage->setFont(font1);
        label_displaymessage->setFrameShape(QFrame::StyledPanel);
        label_displaymessage->setFrameShadow(QFrame::Sunken);
        label_displaymessage->setLineWidth(2);
        label_displaymessage->setAlignment(Qt::AlignCenter);
        label_dexion = new QLabel(widget);
        label_dexion->setObjectName(QString::fromUtf8("label_dexion"));
        label_dexion->setGeometry(QRect(580, 390, 211, 50));
        QSizePolicy sizePolicy(QSizePolicy::Fixed, QSizePolicy::Fixed);
        sizePolicy.setHorizontalStretch(0);
        sizePolicy.setVerticalStretch(0);
        sizePolicy.setHeightForWidth(label_dexion->sizePolicy().hasHeightForWidth());
        label_dexion->setSizePolicy(sizePolicy);
        label_dexion->setMinimumSize(QSize(100, 50));
        QFont font2;
        font2.setPointSize(18);
        font2.setBold(true);
        font2.setWeight(75);
        label_dexion->setFont(font2);
        label_dexion->setPixmap(QPixmap(QString::fromUtf8("../res/dexionlabel.jpg")));
        label_dexion->setScaledContents(true);
        label_dexion->setAlignment(Qt::AlignBottom|Qt::AlignRight|Qt::AlignTrailing);
        label_displayerror = new QLabel(widget);
        label_displayerror->setObjectName(QString::fromUtf8("label_displayerror"));
        label_displayerror->setGeometry(QRect(90, 10, 620, 40));
        label_displayerror->setFont(font1);
        label_displayerror->setFrameShape(QFrame::NoFrame);
        label_displayerror->setFrameShadow(QFrame::Plain);
        label_displayerror->setAlignment(Qt::AlignCenter);
        Keyboard->setCentralWidget(widget);
        menuBar = new QMenuBar(Keyboard);
        menuBar->setObjectName(QString::fromUtf8("menuBar"));
        menuBar->setGeometry(QRect(0, 0, 800, 26));
        menuFile = new QMenu(menuBar);
        menuFile->setObjectName(QString::fromUtf8("menuFile"));
        Keyboard->setMenuBar(menuBar);

        menuBar->addAction(menuFile->menuAction());
        menuFile->addAction(action_quit);

        retranslateUi(Keyboard);

        QMetaObject::connectSlotsByName(Keyboard);
    } // setupUi

    void retranslateUi(QMainWindow *Keyboard)
    {
        Keyboard->setWindowTitle(QApplication::translate("Keyboard", "ZDM Keyboard", 0, QApplication::UnicodeUTF8));
        action_quit->setText(QApplication::translate("Keyboard", "Quit", 0, QApplication::UnicodeUTF8));
        action_quit->setShortcut(QApplication::translate("Keyboard", "Ctrl+Q", 0, QApplication::UnicodeUTF8));
        actionEnable_Maintenance_Mode->setText(QApplication::translate("Keyboard", "Enable Maintenance Mode", 0, QApplication::UnicodeUTF8));
        button_cancel->setText(QString());
        label_cancel->setText(QApplication::translate("Keyboard", "CANCEL", 0, QApplication::UnicodeUTF8));
        label_suspend->setText(QApplication::translate("Keyboard", "SUSPEND", 0, QApplication::UnicodeUTF8));
        button_1->setText(QApplication::translate("Keyboard", "1", 0, QApplication::UnicodeUTF8));
        button_2->setText(QApplication::translate("Keyboard", "2", 0, QApplication::UnicodeUTF8));
        label_link->setText(QApplication::translate("Keyboard", "LINK", 0, QApplication::UnicodeUTF8));
        button_3->setText(QApplication::translate("Keyboard", "3", 0, QApplication::UnicodeUTF8));
        label_unlink->setText(QApplication::translate("Keyboard", "UNLINK", 0, QApplication::UnicodeUTF8));
        button_4->setText(QApplication::translate("Keyboard", "4", 0, QApplication::UnicodeUTF8));
        label_test->setText(QApplication::translate("Keyboard", "TEST", 0, QApplication::UnicodeUTF8));
        button_5->setText(QApplication::translate("Keyboard", "5", 0, QApplication::UnicodeUTF8));
        label_enter->setText(QApplication::translate("Keyboard", "ENTER", 0, QApplication::UnicodeUTF8));
        label_start->setText(QApplication::translate("Keyboard", "START", 0, QApplication::UnicodeUTF8));
        button_enter->setText(QString());
        button_numerals->setText(QString());
        label_numerals->setText(QApplication::translate("Keyboard", "NUMERALS", 0, QApplication::UnicodeUTF8));
        button_6->setText(QApplication::translate("Keyboard", "6", 0, QApplication::UnicodeUTF8));
        label_next->setText(QApplication::translate("Keyboard", "NEXT", 0, QApplication::UnicodeUTF8));
        button_7->setText(QApplication::translate("Keyboard", "7", 0, QApplication::UnicodeUTF8));
        label_unload->setText(QApplication::translate("Keyboard", "UNLOAD", 0, QApplication::UnicodeUTF8));
        button_8->setText(QApplication::translate("Keyboard", "8", 0, QApplication::UnicodeUTF8));
        button_9->setText(QApplication::translate("Keyboard", "9", 0, QApplication::UnicodeUTF8));
        button_0->setText(QApplication::translate("Keyboard", "0", 0, QApplication::UnicodeUTF8));
        label_relight->setText(QApplication::translate("Keyboard", "RELIGHT", 0, QApplication::UnicodeUTF8));
        label_reprn->setText(QApplication::translate("Keyboard", "REPRN", 0, QApplication::UnicodeUTF8));
        label_product->setText(QApplication::translate("Keyboard", "PRODUCT", 0, QApplication::UnicodeUTF8));
        label_displaymessage->setText(QString());
        label_dexion->setText(QString());
        label_displayerror->setText(QString());
        menuFile->setTitle(QApplication::translate("Keyboard", "File", 0, QApplication::UnicodeUTF8));
    } // retranslateUi

};

namespace Ui {
    class Keyboard: public Ui_Keyboard {};
} // namespace Ui

QT_END_NAMESPACE

#endif // UI_KEYBOARD_H
