#include "translator.h"
#include "config.h"
#include "utils.h"

Translator::Translator(QObject* parent) :
    QObject(parent)
{
    rds_mode = Config::instance()->zdm()->rds();
    bus_mode = Config::instance()->zdm()->data_bus();
}

Translator::~Translator()
{
}


QByteArray Translator::translateHostMessage(const QByteArray& msg,
                                            unsigned char stx,
                                            unsigned char etx)
{
    logger()->debug("TRANSLATING MESSAGE hex:%1, ascii:%2",
        QString(msg.toHex()), QString(msg));
    QByteArray cs;
    // Greg. need to translate between old and new RDS and Lights

    cs.append(0xff);
    int x = msg.indexOf(0xf4,1);
    if(rds_mode == 2){
        int i = msg.indexOf(0xf9,x);
        cs.append(msg.mid(x+1, i-x + 1));//moving msg from F4 to F9 + 1 for Id into cs
        Utils::instance()->setChecksum(cs);
    }else{
        int i = msg.indexOf(0xf5,x);
        cs.append(msg.mid(x+1, i-x-1));
        cs.append(0xf9);
        Utils::instance()->appendAdjustedValue(cs, cs.size()-2);
    }

    cs.append(etx);

    logger()->debug("RETURN MESSAGE hex:%1, ascii:%2",
                    QString(cs.toHex()), QString(cs));
    return cs;
}

QByteArray Translator::translateLightMessage(const QByteArray& msg)
{
    logger()->debug("TRANSLATING MESSAGE hex:%1, ascii:%2",
        QString(msg.toHex()), QString(msg));
//    int msg_head_len = 0;
    QByteArray cs;
    unsigned char c;
    unsigned char nextc;

    cs.append(0xf4);
    if(bus_mode == "yes"){
        int i = msg.indexOf(0xf9,1);
        cs.append(msg.mid(1, i+1));
//        cs.append(0xf5);
//        cs.append(msg.mid(i, 2));
    }else{
        int i = msg.indexOf(0xf9,1);
        if (i > 0){
            cs.append(msg.mid(1, i-1));
        }else{
            i = msg.indexOf(0xfe,1);
            cs.append(msg.mid(1, i-1));
        }
        cs.append(0xf5);
    }
    logger()->debug("RETURN MESSAGE hex:%1, ascii:%2",
                    QString(cs.toHex()), QString(cs));
    return cs;

}
