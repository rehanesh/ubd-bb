#ifndef SIBLINGZDM_H
#define SIBLINGZDM_H

#include "config.h"
#include "device.h"
#include <QObject>
#include <QString>
#include <QByteArray>
#include <log4qt/logmanager.h>

class SiblingZdm : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(SiblingZdm)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit SiblingZdm(ConfigSiblingZdm* pSiblingZdm, QObject* parent = 0);
    ~SiblingZdm();

    Device* device();
    void transmit(const QByteArray& msg);

signals:
    void messageReceived(const QByteArray& msg);

private slots:
    void receive(const QByteArray& ms);

private:
    Device* m_pDevice;
};

#endif // SIBLINGZDM_H
