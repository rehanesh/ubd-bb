#include "scanner.h"

Scanner::Scanner(ConfigScanner* pScanner, QObject* parent) :
    QObject(parent)
{
    m_pDevice = new Device(pScanner->device());
    connect(m_pDevice, SIGNAL(messageReceived(QByteArray)),
            this, SLOT(receive(QByteArray)));
}

Scanner::~Scanner()
{
    delete m_pDevice;
    m_pDevice = 0;
}

void Scanner::receive(const QByteArray& msg)
{
    logger()->info("RECEIVED FROM SCANNER hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));
    // remove stxchar and etxchar
    QByteArray cs;
    cs.append(0xfc);
    cs.append(msg.mid(1, msg.size()-2));
    emit messageReceived(cs);
}
