#ifndef SOCKETCLIENT_H
#define SOCKETCLIENT_H

#include <QtCore>
#include <QString>
#include <QTcpSocket>
#include <log4qt/logger.h>

class SocketClient : public QTcpSocket
{
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    SocketClient(QString ip, QString port);
    SocketClient(QString ip, QString port,bool heartbeat_timer);
    ~SocketClient();

    QString ip();
    long port();
    QByteArray data();
    bool isConnected();


signals:
    void GuimessageReceived(const QByteArray& msg);

public slots:
    void opens();
    void socketConnected();
    void serverTimeout();
    void socketDisconnected();

private slots:
    void handleSocketError(QAbstractSocket::SocketError error);

private:
    QString m_ip;
    long m_port;
    QTimer* heartbeatTimer;
    bool enable_hbt;
    bool host_connected;

};

#endif // SOCKETCLIENT_H
