#ifndef ZDM_H
#define ZDM_H

#include "host.h"
#include "hostGui.h"
#include "hostRDS.h"
//#include "keyboard.h"
#include "translator.h"
#include "siblingzdm.h"
#include "light.h"
#include "converter.h"
#include "powerbase.h"
#include "scanner.h"
#include "gpio.h"
#include "beeper.h"
#include <QObject>
#include <QString>
#include <QByteArray>
#include <log4qt/logmanager.h>

class Zdm : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Zdm)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit Zdm(QObject* parent = 0);
    ~Zdm();

signals:
    void display(const QString& msg);
    void displayError(const QString& msg);
    void displayLed(int, int);

public slots:

    void handleKeyboardMessage(const QByteArray& msg);
    void handleErrorMessage(const QByteArray& msg);

private slots:
    void handleHostMessage(const QByteArray& msg);
    int handleF9(const QByteArray& msg);
    void handleNextSiblingZdmMessage(const QByteArray& msg);
    void handlePowerbaseMessage(const QByteArray& msg);
    void handleScannerMessage(const QByteArray& msg);
    void handleLightMessage(const QByteArray& msgin);
    void handleLightNoResponse(const QByteArray& msg);
private:
//    bool isKeyword(unsigned char c);
//    void appendChecksum(QByteArray& cs,
//                        unsigned char stx,
//                        unsigned char etx);
//    void appendAdjustedValue(QByteArray& cs, int n);
    void beep(int times);
    QByteArray buildHostMessage(const QByteArray& msg);
    void appendAdjustedValue(QByteArray& cs, int n);
    void transmitToHost(const QByteArray& msg);
    void sendReset();
    bool TX_next;
    unsigned char m_addr;
    unsigned char rds_mode;
    QString virtual_3dd;
    QString bus_mode;
    Host* m_pHost;
    HostRDS* m_pHostRDS;
    HostGui* m_pHostGui;
    SiblingZdm* m_pPreviousSiblingZdm;
    SiblingZdm* m_pNextSiblingZdm;
    Powerbase* m_pPowerbase;
    Light* m_pLight;
    Scanner* m_pScanner;
    Translator* m_pTranslator;
    Converter* m_pConverter;
    Gpio* m_pGpio;
    QByteArray lightMsg;

    Beeper* m_pBeeper;
};

#endif // ZDM_H
