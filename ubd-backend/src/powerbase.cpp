#include "powerbase.h"

Powerbase::Powerbase(ConfigPowerbase* pPowerbase, QObject* parent) :
    QObject(parent)
{
    m_addr = pPowerbase->addr();
    m_packetId = 0;
    m_pRepeater = new Repeater(pPowerbase->repeater());
    m_pDevice = new Device(pPowerbase->device());
    connect(m_pDevice, SIGNAL(messageReceived(QByteArray)),
            this, SLOT(receive(QByteArray)));
}

Powerbase::~Powerbase()
{
    delete m_pRepeater;
    m_pRepeater = 0;

    delete m_pDevice;
    m_pDevice = 0;
}

unsigned char Powerbase::addr()
{
    return m_addr;
}

unsigned char Powerbase::packetId()
{
    if(m_packetId == 236)
    {
        m_packetId = 1;
    }
    else
    {
        m_packetId ++;
    }
    return m_packetId;
}

Device* Powerbase::device()
{
    return m_pDevice;
}

Repeater* Powerbase::repeater()
{
    return m_pRepeater;
}

void Powerbase::transmit(const QByteArray& msg)
{
    logger()->info("SENDING TO POWERBASE hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));
    m_pDevice->write(msg);
}

void Powerbase::receive(const QByteArray& msg)
{
    logger()->info("RECEIVED FROM POWERBASE hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));
    emit messageReceived(msg);
}
