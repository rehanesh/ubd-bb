#ifndef BEEPER_H
#define BEEPER_H

#include <QObject>
#include <QFile>
//#include <QAudioOutput>
#include <log4qt/logmanager.h>
#include "gpio.h"
#include <QTimer>



class Beeper : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Beeper)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit Beeper(QObject* parent = 0);
    ~Beeper();

    void play(int times);

private slots:
//    void stop(QAudio::State state);
    void Timeout();


    //void notified();

private:
    void start();
    void stop();


//    QFile* m_pAudioFile;
//    QAudioOutput* m_pAudioOutput;
    int m_times;
    QTimer* pTimer;
     Gpio* pGpio;
     bool beeper;
};

#endif // BEEPER_H
