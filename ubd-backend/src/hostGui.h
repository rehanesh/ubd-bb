#ifndef HostGui_H
#define HostGui_H

#include "config.h"
#include "device.h"
#include "gpio.h"
#include <QObject>
#include <QString>
#include <QByteArray>
#include <QTimer>
#include <deque>
#include <log4qt/logmanager.h>

class HostGui : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(HostGui)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit HostGui(ConfigHostGui* pHostGui, QObject* parent = 0);
    ~HostGui();

    QTimer* timer();
    QTimer* RTStimer();
    Device* device();
    void loadTXbuff(const QByteArray& msg, bool check);
    void checkRTS();
    std::deque <QByteArray> message_buff;
    bool waiting_OK;
    bool TXing;
//    QString RTSEnable;

signals:
    void messageReceived(const QByteArray& msg);
    void errorReceived(const QByteArray& msg);
    void SendToRDS(const QByteArray& msg);


public slots:

private slots:
      void handleRDSMessage(const QByteArray& msg);
      void handleMessagesFromGui(const QByteArray& msg);
      void displayMessage(const QString& msg);
      void displayErrorMessage(const QString& msg);
//    void receive(const QByteArray& msg);
//    void transmit();
//    void reTransmit();
//    void checkCTS();
//    void test();

private:
    QTimer* m_pTimer;
    QTimer* m_pRTSTimer;
    Device* m_pDevice;
    Gpio* m_pGpio;
    QByteArray m_message;
    int m_retries;
    int rts_retries;
};

#endif // HostGui_H
