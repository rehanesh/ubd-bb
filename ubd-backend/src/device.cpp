#include "device.h"

#define DEVICE_TYPE_SERIAL "SERIAL"
#define DEVICE_TYPE_SOCKET "SOCKET"

Device::Device(ConfigDevice* pDevice, QObject* parent) :
    QObject(parent)
{
    m_pSerial = 0;
    m_pSocket = 0;
    string_buf ="";
    reset_sent = false;
    m_stx = pDevice->stxchar();
    logger()->warn("Returned STX %1 .", m_stx);
    m_etx = pDevice->etxchar();
    logger()->warn("Returned ETX %1 .", m_etx);
    m_rtscontrol = pDevice->rtscontrol();

    QString type = pDevice->type().toUpper();
    if (type == DEVICE_TYPE_SERIAL)
    {
        m_pSerial = new SerialPort(pDevice->port());
        m_pSerial->setBaudRate(
                    SerialPort::serialBaudRate(pDevice->baudrate()));
        m_pSerial->setDataBits(
                    SerialPort::serialDataBits(pDevice->databits()));
        m_pSerial->setParity(
                    SerialPort::serialParity(pDevice->parity()));
        m_pSerial->setStopBits(
                    SerialPort::serialStopBits(pDevice->stopbits()));
        m_pSerial->setFlowControl(
                    SerialPort::serialFlowControl(pDevice->flowcontrol()));
        m_pSerial->setTimeout(pDevice->timeout());
        if (m_pSerial->opens(SerialPort::serialOpenMode(pDevice->openmode())))
        {
            connect(m_pSerial, SIGNAL(readyRead()), this, SLOT(read()));
        }
    }
    else if (type == DEVICE_TYPE_SOCKET)
    {
        int pos = pDevice->port().indexOf(":");
        if (pos <= 0)
        {
            logger()->error("config.xml: device port wrong format.");
            return;
        }

        QString socket_type = pDevice->Sockettype().toUpper();
        if(socket_type == "CLIENT_GUI"){

       logger()->debug("Establishing a tcp connection for GUI messages");

        m_pSocket = new SocketClient(pDevice->port().left(pos),
                                     pDevice->port().mid(pos+1),true);
        m_pSocket->opens();
        connect(m_pSocket, SIGNAL(connected()), m_pSocket, SLOT(socketConnected()));

        connect(m_pSocket, SIGNAL(readyRead()), this, SLOT(readGuiMessages()));

        }else if(socket_type == "CLIENT_LIGHT"){

        logger()->debug("Establishing a tcp connection for LIGHT messages");

        m_pSocket = new SocketClient(pDevice->port().left(pos),
                                     pDevice->port().mid(pos+1));
        m_pSocket->opens();
        connect(m_pSocket, SIGNAL(readyRead()), this, SLOT(read()));
    }else if (socket_type == "SERVER"){
             m_pServerSocket = new SocketServer(pDevice->port().mid(pos+1));
             m_pServerSocket->wait_for_connections();
             connect(m_pServerSocket->getServer(), SIGNAL(newConnection()), this, SLOT(onNewConnection()));

        }else{
            logger()->error("config.xml: missing Socket Type ");
            return;
        }
    }
    else
    {
        logger()->error("config.xml: missing device or device type.");
        return;
    }

    mainTimer = new QTimer(this);
    mainTimer->setInterval(1000);
    connect(mainTimer, SIGNAL(timeout()), this, SLOT(checkTimer()));
    mainTimer->setSingleShot(false);
    mainTimer->start();


}

Device::~Device()
{
    if (m_pSerial)
    {
        delete m_pSerial;
        m_pSerial = 0;
    }
    if (m_pSocket)
    {
        delete m_pSocket;
        m_pSocket = 0;
    }
}

unsigned char Device::stx()
{
    return m_stx;
}

unsigned char Device::etx()
{
    return m_etx;
}

QString Device::rtscontrol()
{
    return m_rtscontrol;
}

void Device::SendtoGui(const QByteArray& msg){

   //logger()->error("Sending %1 to GUI ", QString(msg));
   if(m_pServerSocket)
   m_pServerSocket->write(msg);

}

void Device::write(const QByteArray& msg)
{
    //logger()->error("Writing to Socket ");
    if (m_pSerial)
    {
        if (m_pSerial->isOpen())
        {
            m_pSerial->write(msg);
            m_pSerial->flush();
        }
    }
    else if (m_pSocket)
    {
        if (m_pSocket->state() == QTcpSocket::ConnectedState)
        {
            m_pSocket->write(msg);
            m_pSocket->flush();
        }
        else
        {
            logger()->error("%1:%2 is not connected yet state %3",
                            m_pSocket->ip(),
                            QString::number(m_pSocket->port()),
                            m_pSocket->state());
        }
    }
    logger()->error("Writing to Socket End ");

}

void Device::read()
{
    QByteArray data;
    while (true)
    {
        if (m_pSerial)
        {
            data = m_pSerial->data();
        }
        else if (m_pSocket)
        {
            data = m_pSocket->data();
        }
        if (!data.isEmpty())
        {
            m_buffer.append(data);
            //logger()->debug("DEVICE RECEIVED %1, %2",
              //              QString(data.toHex()), QString(data));
        }
        else
        {
            break;
        }
    }
    // get message packet from m_buffer
    QByteArray msg;
    while (!m_buffer.isEmpty())
    {

        int etxPos = m_buffer.indexOf(m_etx);
        if (etxPos < 0)
        {
            logger()->warn("no %1 found in %2.", m_etx, QString(m_buffer.toHex()));
            break;
        }

        //int stxPos = m_buffer.indexOf(m_stx,0);
        int stxPos = etxPos;
        while(stxPos >= 0 && (unsigned char)m_buffer[stxPos] != m_stx)
            stxPos--;

        if (stxPos < 0)
        {
            logger()->warn("no %1 found in %2.", m_stx, QString(m_buffer.toHex()));
 logger()->warn("%1 position is %2. discarding unknown data %3.",
                           QString(m_etx), QString(etxPos),
                           QString(m_buffer.left(etxPos).toHex()));
           
        	m_buffer.remove(0, etxPos + 1);
            continue;
        }
        int len = 0;
        if (stxPos > 0)
        {
            len = stxPos;
            logger()->warn("%1 position is %2. discarding unknown data %3.",
                           QString(m_stx), QString(stxPos),
                           QString(m_buffer.left(len).toHex()));
            m_buffer.remove(0, len);
            stxPos = 0;
            etxPos -= len;
        }
        len = etxPos - stxPos + 1;
        msg = m_buffer.left(len);
        m_buffer.remove(0, len);
        logger()->warn("%1 received. remaining %2.",
                       QString(msg.toHex()), QString(m_buffer.toHex()));
        emit messageReceived(msg);
    }
}

/*TCP Client Reading (GUI) messages from RDS */

void Device::readGuiMessages()
{
    QByteArray data;
    while (true)
    {
        if (m_pSocket)
        {
            data = m_pSocket->data();
        }

        if (!data.isEmpty())
        {
            string_buf = string_buf + data;
        }
        else
        {
            break;
        }
    }
 QByteArray msg ;
 QString string_msg;
 logger()->debug("DEVICE RECEIVED %1",string_buf);

 while (!string_buf.isEmpty())
 {

     int startPos = string_buf.indexOf("START|");
     int endPos = string_buf.indexOf("|END");

     logger()->debug("START Pos %1",startPos);
          logger()->debug("END Pos %1",endPos);

          if (startPos < 0)
          {
              logger()->warn("no START found in %1.",string_buf);
              break;
          }
          if (endPos < 0)
          {
              logger()->warn("no End found in %1.",string_buf);
              break;
          }

          int len = 0;

          len = endPos - startPos + 4;

          logger()->warn("Rehanesh len %1.",len);

          string_msg = string_buf.left(len);
          string_buf.remove(0, len);
          logger()->warn("%1 received. remaining %2.",
                         string_msg, string_buf);

     msg.append(string_msg);
     // sending signal to hostRDS
     emit GuimessageReceivedFromRDS(msg);
 }
     return;
}


/*TCP Server Code  receving messages from GUI and sending to RDS*/

void Device::onNewConnection()
{
   logger()->debug("Client Connected");
   clientSocket = m_pServerSocket->getClientSocket();
   logger()->debug("IP %1",clientSocket->peerAddress().toString());
   m_pServerSocket->setClientConnected(true);
   connect(clientSocket, SIGNAL(readyRead()), this, SLOT(ReadMessagesFromGUI()));
   connect(clientSocket, SIGNAL(disconnected()), this, SLOT(clientDisconnected()));

   connect(clientSocket, SIGNAL(stateChanged(QAbstractSocket::SocketState)), m_pServerSocket, SLOT(onSocketStateChanged(QAbstractSocket::SocketState)));
}
void Device::clientDisconnected(){
    QByteArray msg = "STX|RemoveSession|ETX" ;
    QString string_msg;
    msg.append(string_msg);
    emit SendToRDS(msg);
    m_pServerSocket->setClientConnected(false);
}

void Device::ReadMessagesFromGUI()
{
    QByteArray data;
    logger()->debug("MSG RECEIVED FROM GUI ");

    while (true)
    {
        if (m_pServerSocket)
        {
            data = m_pServerSocket->read();
        }
        if (!data.isEmpty())
        {
            string_buf = string_buf + data;
            logger()->debug("DEVICE RECEIVED %1 %2", QString(data.toHex()), QString(data));
        }
        else
        {
            break;
        }
    }
 QByteArray msg ;
 QString string_msg;
 logger()->debug("MSG RECEIVED FROM GUI %1",string_buf);
 logger()->warn("Returned STX %1 .", m_stx);
 logger()->warn("Returned ETX %1 .", m_etx);

 while (!string_buf.isEmpty())
 {

     int startPos = string_buf.indexOf(m_stx);
     int endPos = string_buf.indexOf(m_etx);

     logger()->debug("START Pos %1",startPos);
     logger()->debug("END Pos %1",endPos);

     if (startPos < 0)
     {
         logger()->warn("no START found in %1.",string_buf);
         break;
     }
     if (endPos < 0)
     {
         logger()->warn("no End found in %1.",string_buf);
         break;
     }

     int len = 0;
     len = endPos - startPos + 4;
     string_msg = string_buf.left(len);
     string_buf.remove(0, len);
     logger()->warn("%1 received. remaining %2",
                    string_msg, string_buf);

     msg.append(string_msg);
     //Sending signal to hostGUI
     emit SendToRDS(msg);

 }
     return;
}

void Device::checkTimer()
{
    if (m_pSocket){
    //logger()->debug(" Checking if Host is alive %1",m_pSocket->isConnected());
    QByteArray msg ;
    QString string_msg;
  if(!m_pSocket->isConnected() && !reset_sent){
      //if RDS host is down , sent reset to GUI Client
      string_msg = "STX|RESET|ETX";
      msg.append(string_msg);
      emit GuimessageReceivedFromRDS(msg);
      reset_sent = true;
   }else if(m_pSocket->isConnected()) reset_sent = false;

    }

}


