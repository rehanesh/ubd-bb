#include "config.h"

#include <QFile>
#include <QIODevice>
#include <log4qt/logmanager.h>

#define CONFIG_FILE_NAME "../conf/config.xml"

// device
ConfigDevice::ConfigDevice(QDomElement e, QObject* parent):
    QObject(parent)
{
    m_stxchar = 0xff;
    m_etxchar = 0xfe;
    if (!e.isNull())
    {
        m_type = e.attribute("type").trimmed();
        QDomElement childe = e.firstChildElement(QString("port"));
        if (!childe.isNull())
        {
            m_port = childe.text().trimmed().toLatin1();
        }
        childe = e.firstChildElement(QString("sockettype"));
        if (!childe.isNull())
        {
            m_sockettype = childe.text().trimmed();
        }
        childe = e.firstChildElement(QString("openmode"));
        if (!childe.isNull())
        {
            m_openmode = childe.text().trimmed();
        }
        childe = e.firstChildElement(QString("baudrate"));
        if (!childe.isNull())
        {
            m_baudrate = childe.text().trimmed();
        }
        childe = e.firstChildElement(QString("databits"));
        if (!childe.isNull())
        {
            m_databits = childe.text().trimmed();
        }
        childe = e.firstChildElement(QString("parity"));
        if (!childe.isNull())
        {
            m_parity = childe.text().trimmed();
        }
        childe = e.firstChildElement(QString("stopbits"));
        if (!childe.isNull())
        {
            m_stopbits = childe.text().trimmed();
        }
        childe = e.firstChildElement(QString("flowcontrol"));
        if (!childe.isNull())
        {
            m_flowcontrol = childe.text().trimmed();
        }
        childe = e.firstChildElement(QString("rtscontrol"));
        if (!childe.isNull())
        {
            m_rtscontrol = childe.text().trimmed();
        }
        childe = e.firstChildElement(QString("timeout"));
        if (!childe.isNull())
        {
            m_timeout = childe.text().trimmed().toLong();
        }
        childe = e.firstChildElement(QString("stxchar"));
        if (!childe.isNull())
        {
            m_stxchar = parseToNumber(
                    childe.text().trimmed().toStdString().c_str()) & 0xff;
        }
        childe = e.firstChildElement(QString("etxchar"));
        if (!childe.isNull())
        {
            m_etxchar = parseToNumber(
                        childe.text().trimmed().toStdString().c_str()) & 0xff;
            logger()->debug("etxchar %1", m_etxchar);
        }
    }
}
ConfigDevice::~ConfigDevice()
{
}
QString ConfigDevice::type()
{
    return m_type;
}
QString ConfigDevice::Sockettype()
{
    return m_sockettype;
}
QString ConfigDevice::port()
{
    return m_port;
}
QString ConfigDevice::openmode()
{
    return m_openmode;
}

QString ConfigDevice::baudrate()
{
    return m_baudrate;
}
QString ConfigDevice::databits()
{
    return m_databits;
}
QString ConfigDevice::parity()
{
    return m_parity;
}
QString ConfigDevice::stopbits()
{
    return m_stopbits;
}
QString ConfigDevice::flowcontrol()
{
    return m_flowcontrol;
}
QString ConfigDevice::rtscontrol()
{
    return m_rtscontrol;
}
long ConfigDevice::timeout()
{
    return m_timeout;
}
unsigned char ConfigDevice::etxchar()
{
    return m_etxchar;
}
unsigned char ConfigDevice::stxchar()
{
//    logger()->debug("Greg1 - stxchar %1", m_stxchar);

    return m_stxchar;
}
int ConfigDevice::parseToNumber(const char* str)
{
    int val;
    int sign;
    int c;

    if(*str == '-') {
        sign = -1;
        str++;
    } else sign = 1;

    val = 0;

    /* check for hex */
    if(str[0] == '0' && (str[1] == 'x' || str[1] == 'X') ) {
        str += 2;
        while (1) {
            c = *str++;
            if(c >= '0' && c <= '9')
                val = (val<<4) + c - '0';
            else if (c >= 'a' && c <= 'f')
                val = (val<<4) + c - 'a' + 10;
            else if (c >= 'A' && c <= 'F')
                val = (val<<4) + c - 'A' + 10;
            else
                return val*sign;
        }
    }
    /* check for character */
    if (str[0] == '\'') {
        return sign * str[1];
    }

    /* assume decimal */
    while (1) {
        c = *str++;
        if(c <'0' || c > '9')
            return val*sign;
        val = val*10 + c - '0';
    }
    return 0;
}

// scanner
ConfigScanner::ConfigScanner(QDomElement e, QObject* parent) :
    QObject(parent)
{
    m_pDevice = 0;
    if (!e.isNull())
    {
        QDomElement childe = e.firstChildElement("device");
        if (!childe.isNull())
        {
            m_pDevice = new ConfigDevice(childe);
        }
    }
}
ConfigScanner::~ConfigScanner()
{
    if (m_pDevice)
    {
        delete m_pDevice;
        m_pDevice = 0;
    }
}
ConfigDevice* ConfigScanner::device()
{
    return m_pDevice;
}

// light
ConfigLight::ConfigLight(QDomElement e, QObject* parent) :
    QObject(parent)
{
    m_pDevice = 0;
    if (!e.isNull())
    {
        QDomElement childe = e.firstChildElement("device");
        if (!e.isNull())
        {
            m_pDevice = new ConfigDevice(childe);
        }
    }
}
ConfigLight::~ConfigLight()
{
    if (m_pDevice)
    {
        delete m_pDevice;
        m_pDevice = 0;
    }
}
ConfigDevice* ConfigLight::device()
{
    return m_pDevice;
}

// repeater
ConfigRepeater::ConfigRepeater(QDomElement e, QObject* parent) :
    QObject(parent)
{
    if (!e.isNull())
    {
        m_addr = e.attribute("addr").trimmed().toInt();
    }
}
ConfigRepeater::~ConfigRepeater()
{
}
int ConfigRepeater::addr()
{
    return m_addr;
}

// powerbase
ConfigPowerbase::ConfigPowerbase(QDomElement e, QObject* parent) :
    QObject(parent)
{
    m_pDevice = 0;
    m_pRepeater = 0;
    if (!e.isNull())
    {
        m_addr = e.attribute("addr").trimmed().toInt();
        QDomElement childe = e.firstChildElement("device");
        if (!childe.isNull())
        {
            m_pDevice = new ConfigDevice(childe);
        }
        childe = e.firstChildElement("repeater");
        if (!childe.isNull())
        {
            m_pRepeater = new ConfigRepeater(childe);
        }
    }
}
ConfigPowerbase::~ConfigPowerbase()
{
    if (m_pDevice)
    {
        delete m_pDevice;
        m_pDevice = 0;
    }
    if (m_pRepeater)
    {
        delete m_pRepeater;
        m_pRepeater = 0;
    }
}
int ConfigPowerbase::addr()
{
    return m_addr;
}
ConfigDevice* ConfigPowerbase::device()
{
    return m_pDevice;
}
ConfigRepeater* ConfigPowerbase::repeater()
{
    return m_pRepeater;
}

// previoussiblingzdm, nextsiblingzdm
ConfigSiblingZdm::ConfigSiblingZdm(QDomElement e, QObject* parent) :
    QObject(parent)
{
    m_pDevice = 0;
    if (!e.isNull())
    {
        QDomElement childe = e.firstChildElement("device");
        if (!e.isNull())
        {
            m_pDevice = new ConfigDevice(childe);
        }
    }
}
ConfigSiblingZdm::~ConfigSiblingZdm()
{
    if (m_pDevice)
    {
        delete m_pDevice;
        m_pDevice = 0;
    }
}
ConfigDevice* ConfigSiblingZdm::device()
{
    return m_pDevice;
}

// host
ConfigHost::ConfigHost(QDomElement e, QObject* parent) :
    QObject(parent)
{
    m_pDevice = 0;
    if (!e.isNull())
    {
        QDomElement childe = e.firstChildElement("device");
        if (!e.isNull())
        {
            m_pDevice = new ConfigDevice(childe);
        }
    }
}


ConfigHost::~ConfigHost()
{
    if (m_pDevice)
    {
        delete m_pDevice;
        m_pDevice = 0;
    }
}
ConfigDevice* ConfigHost::device()
{
    return m_pDevice;
}

// host RDS
ConfigHostRDS::ConfigHostRDS(QDomElement e, QObject* parent) :
    QObject(parent)
{
    m_pDevice = 0;
    if (!e.isNull())
    {
        QDomElement childe = e.firstChildElement("device");
        if (!e.isNull())
        {
            m_pDevice = new ConfigDevice(childe);
        }
    }
}


ConfigHostRDS::~ConfigHostRDS()
{
    if (m_pDevice)
    {
        delete m_pDevice;
        m_pDevice = 0;
    }
}
ConfigDevice* ConfigHostRDS::device()
{
    return m_pDevice;
}



// host Gui
ConfigHostGui::ConfigHostGui(QDomElement e, QObject* parent) :
    QObject(parent)
{
    m_pDevice = 0;
    if (!e.isNull())
    {
        QDomElement childe = e.firstChildElement("device");
        if (!e.isNull())
        {
            m_pDevice = new ConfigDevice(childe);
        }
    }
}


ConfigHostGui::~ConfigHostGui()
{
    if (m_pDevice)
    {
        delete m_pDevice;
        m_pDevice = 0;
    }
}
ConfigDevice* ConfigHostGui::device()
{
    return m_pDevice;
}

// zdm
ConfigZdm::ConfigZdm(QDomElement e, QObject* parent) :
    QObject(parent)
{
    m_pHost = 0;
    m_pHostRDS = 0;
    m_pHostGui = 0;
    m_pPreviousSiblingZdm = 0;
    m_pNextSiblingZdm = 0;
    m_pPowerbase = 0;
    m_pLight = 0;
    m_pScanner = 0;
    if (!e.isNull())
    {
        m_addr = e.attribute("addr").trimmed().toInt();
        m_rds = e.attribute("rds_ver").trimmed().toInt();
        m_version = e.attribute("version").trimmed();
        m_virtual3dd = e.attribute("virtual_3dd").trimmed();
        m_data_bus = e.attribute("data_bus").trimmed();
        m_flip_layout = e.attribute("flip_layout").trimmed();
        QDomElement childe = e.firstChildElement("host");
        if (!childe.isNull())
        {
            m_pHost = new ConfigHost(childe);
        }
        childe = e.firstChildElement("hostRDS");
        if (!childe.isNull())
        {
            m_pHostRDS = new ConfigHostRDS(childe);
        }

        childe = e.firstChildElement("hostGui");
        if (!childe.isNull())
        {
            m_pHostGui = new ConfigHostGui(childe);
        }

        childe = e.firstChildElement("previoussiblingzdm");
        if (!childe.isNull())
        {
            m_pPreviousSiblingZdm = new ConfigSiblingZdm(childe);
        }
        childe = e.firstChildElement("nextsiblingzdm");
        if (!childe.isNull())
        {
            m_pNextSiblingZdm = new ConfigSiblingZdm(childe);
        }
        childe = e.firstChildElement("powerbase");
        if (!childe.isNull())
        {
            m_pPowerbase = new ConfigPowerbase(childe);
        }
        childe = e.firstChildElement("light");
        if (!childe.isNull())
        {
            m_pLight = new ConfigLight(childe);
        }
        childe = e.firstChildElement("scanner");
        if (!childe.isNull())
        {
            m_pScanner = new ConfigScanner(childe);
        }
    }
}
ConfigZdm::~ConfigZdm()
{
    if (m_pHost)
    {
        delete m_pHost;
        m_pHost = 0;
    }
    if (m_pHostRDS)
    {
        delete m_pHostRDS;
        m_pHostRDS = 0;
    }
    if (m_pHostGui)
    {
        delete m_pHostGui;
        m_pHostGui = 0;
    }
    if (m_pPreviousSiblingZdm)
    {
        delete m_pPreviousSiblingZdm;
        m_pPreviousSiblingZdm = 0;
    }
    if (m_pNextSiblingZdm)
    {
        delete m_pNextSiblingZdm;
        m_pNextSiblingZdm = 0;
    }
    if (m_pPowerbase)
    {
        delete m_pPowerbase;
        m_pPowerbase = 0;
    }
    if (m_pLight)
    {
        delete m_pLight;
        m_pLight = 0;
    }
    if (m_pScanner)
    {
        delete m_pScanner;
        m_pScanner = 0;
    }
}
int ConfigZdm::addr()
{
    return m_addr;
}
int ConfigZdm::rds()
{
    return m_rds;
}
QString ConfigZdm::data_bus()
{
    return m_data_bus;
}
QString ConfigZdm::virtual3dd()
{
    return m_virtual3dd;
}
QString ConfigZdm::flip_layout()
{
    return m_flip_layout;
}
QString ConfigZdm::version()
{
    return m_version;
}
ConfigHost* ConfigZdm::host()
{
    return m_pHost;
}
ConfigHostRDS* ConfigZdm::hostRDS()
{
    return m_pHostRDS;
}
ConfigHostGui* ConfigZdm::hostGui()
{
    return m_pHostGui;
}
ConfigSiblingZdm* ConfigZdm::previoussiblingzdm()
{
    return m_pPreviousSiblingZdm;
}
ConfigSiblingZdm* ConfigZdm::nextsiblingzdm()
{
    return m_pNextSiblingZdm;
}
ConfigPowerbase* ConfigZdm::powerbase()
{
    return m_pPowerbase;
}
ConfigLight* ConfigZdm::light()
{
    return m_pLight;
}
ConfigScanner* ConfigZdm::scanner()
{
    return m_pScanner;
}

// config
Config* Config::m_pInstance = 0;
Config* Config::instance()
{
    if (!m_pInstance)
    {
        m_pInstance = new Config;
    }
    return m_pInstance;
}
Config::Config(QObject* parent) :
    QObject(parent)
{
    m_pZdm = 0;
    QDomDocument doc;
    QFile file(CONFIG_FILE_NAME);
    if (!file.open(QIODevice::ReadOnly))
    {
        logger()->error("cannot open config file.");
        return;
    }
    if (!doc.setContent(&file))
    {
         logger()->error("cannot load config file to dom.");
        file.close();
        return;
    }
    file.close();
    QDomElement e = doc.documentElement();
    if (!e.isNull())
    {
        m_pZdm = new ConfigZdm(e);
    }
}
Config::~Config()
{
    if (m_pInstance)
    {
        delete m_pInstance;
        m_pInstance = 0;
    }
    if (m_pZdm)
    {
        delete m_pZdm;
        m_pZdm = 0;
    }
}
ConfigZdm* Config::zdm()
{
    return m_pZdm;
}
