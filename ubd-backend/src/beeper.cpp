#include "beeper.h"
#include "log4qt/logger.h"
#include <QFile>
#include <QByteArray>
//#include <QAudio>
//#include <QAudioFormat>
//#include <QAudioDeviceInfo>

#define BEEP_WAV "../res/beep.wav"
#define BEEPER "/sys/class/gpio/gpio9/" //output

Beeper::Beeper(QObject* parent) :
    QObject(parent)
{
    pGpio = new Gpio();
    pTimer = new QTimer(this);
    pTimer->setInterval(500);
    connect(pTimer, SIGNAL(timeout()), this, SLOT(Timeout()));
    beeper = false;
//    m_pAudioFile = new QFile(BEEP_WAV);
//    if (m_pAudioFile->open(QIODevice::ReadOnly))
//    {
//         QAudioFormat format;
//         format.setFrequency(44100);
//         format.setChannels(1);
//         format.setSampleSize(16);
//         format.setCodec("audio/pcm");
//         format.setByteOrder(QAudioFormat::LittleEndian);
//         format.setSampleType(QAudioFormat::SignedInt);

//         QAudioDeviceInfo info(QAudioDeviceInfo::defaultOutputDevice());
//         if (info.isFormatSupported(format))
//         {
//             logger()->debug("audio device name %1", info.deviceName());
//             m_pAudioOutput = new QAudioOutput(info, format);
//             m_pAudioOutput->setNotifyInterval(100);
//             connect(m_pAudioOutput, SIGNAL(stateChanged(QAudio::State)),
//                     SLOT(stop(QAudio::State)));
//         }
//         else
//         {
//             logger()->error("raw audio format not supported by backend.");
//         }
//    }
}

Beeper::~Beeper()
{
    delete pGpio;
    pGpio = 0;
//    if (m_pAudioOutput)
//    {
//        delete m_pAudioOutput;
//        m_pAudioOutput = 0;
//    }

//    if (m_pAudioFile->isOpen())
//    {
//        m_pAudioFile->close();
//    }
//    delete m_pAudioFile;
//    m_pAudioFile = 0;
}

void Beeper::play(int times)
{
    logger()->debug("play() times %1", times);
    m_times = times;
    this->start();
}

void Beeper::start()
{
    //logger()->debug("start playing audio");
    logger()->debug("Turning on");
    beeper = true;
    pGpio->output(BEEPER, "0");
    pTimer->setSingleShot(true);
    pTimer->start();
    return;

}

//void Beeper::stop(QAudio::State state)
void Beeper::stop()
{
    //logger()->debug("stop playing audio, state %1", state);
//    if (m_pAudioOutput && state == QAudio::IdleState)
//    {
//        m_pAudioOutput->stop();
//        m_times --;
//        if (m_times > 0)
//        {
//            this->start();
//        }
//    }
    logger()->debug("Turning off");

    beeper = false;
    pGpio->output(BEEPER, "1");
    pTimer->setSingleShot(true);
    pTimer->start();
    return;
}
void Beeper::Timeout()// off 500 mS timer
{
    if (m_times < 0)
    {
        logger()->debug("Stopping Beeper");
        pGpio->output(BEEPER, "1");
        return;

    }
    m_times --;
    if(beeper) this->stop();
    else this->start();

}
