#include "utils.h"

Utils* Utils::m_pInstance = 0;

Utils::Utils(QObject* parent) :
    QObject(parent)
{
    m_keywords.resize(16);
    m_keywords[0] = 0xf0;
    m_keywords[1] = 0xf1;
    m_keywords[2] = 0xf2;
    m_keywords[3] = 0xf3;
    m_keywords[4] = 0xf4;
    m_keywords[5] = 0xf5;
    m_keywords[6] = 0xf6;
    m_keywords[7] = 0xf7;
    m_keywords[8] = 0xf8;
    m_keywords[9] = 0xf9;
    m_keywords[10] = 0xfa;
    m_keywords[11] = 0xfb;
    m_keywords[12] = 0xfc;
    m_keywords[13] = 0xfd;
    m_keywords[14] = 0xfe;
    m_keywords[15] = 0xff;
}

Utils::~Utils()
{
    delete m_pInstance;
    m_pInstance = 0;
}

Utils* Utils::instance()
{
    if (!m_pInstance)
    {
       m_pInstance = new Utils;
    }
    return m_pInstance;
}

bool Utils::isKeyword(unsigned char c)
{
    for (int i=0; i<m_keywords.size(); i++)
    {
        unsigned char temp = m_keywords[i];
        if (c == temp)
        {
            return true;
        }
    }
    return false;
}

/**
 * @brief converter::appendadjustedvalue
 * @param cs
 * @param n
 * dec:15->hex:15
 */
void Utils::appendAdjustedValue(QByteArray& cs, int n)
{
    if (n < 0)
    {
        logger()->warn("cannot append negative value");
    }
    else if (n < 10)
    {
        cs.append(n);
    }
    else
    {
        QByteArray a;
        a.setNum(n);
        logger()->debug("a size %1", a.size());
        int value = (a.at(0) - 0x30) * 16 + a.at(1) - 0x30;
        logger()->debug("value %1", value);
        cs.append(value);
    }
}

void Utils::setChecksum(QByteArray& msg){

    unsigned char* data = (unsigned char*)(msg.data());
    unsigned char sum = 0xff;
    int i;
    for (i=1; i<msg.size(); i++)
    {
       sum += data[i];
    }
    sum += 0xfe;
    sum = 0xff - sum + 1;
    unsigned char temp = sum >> 4;
    if (temp > 9)
    {
        msg.append(temp+0x37);
    }
    else
    {
        msg.append(temp+0x30);
    }
    temp = sum & 0x0f;
    if (temp > 9)
    {
        msg.append(temp+0x37);
    }
    else
    {
        msg.append(temp+0x30);
    }
}

int verifyChecksum(const QByteArray& msg){
    unsigned char sum = 0x00;
    int i;
    for (i=1; i<msg.size(); i++)
    {
       sum += msg[i];
    }
    return sum;
}
