#include "display.h"

display::display(QObject* parent) :
    QObject(parent)
{
}

display::~display()
{
}

void display::errorMessage(const QString &msg){
    emit displayError(msg);
}
