#ifndef SocketServer_H
#define SocketServer_H

//#include <QSocketServer>
#include <QTcpServer>
#include <QTcpSocket>
#include <log4qt/logger.h>

namespace Ui {
class SocketServer;
}

class SocketServer : public QObject

{
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    SocketServer(QString port);
    //explicit SocketServer(QObject *parent = 0);
    ~SocketServer();
    void write(const QByteArray& msg);
    void wait_for_connections();
    void setClientConnected(bool value);
    QByteArray read();
    QTcpSocket* getClientSocket();
    QTcpServer* getServer();


signals:
    //void SendToRDS(const QByteArray& msg);

public slots:
   // void onNewConnection();
    void onSocketStateChanged(QAbstractSocket::SocketState socketState);
    //void onReadyRead();
private:
    QTcpServer  *_server;
    QList<QTcpSocket*>  _sockets;
    QTcpSocket *clientSocket;
    long m_port;
    bool client_connected;
};

#endif // SocketServer_H
