#include "light.h"
#include "config.h"
#include <QDebug>

Light::Light(ConfigLight* pLight, QObject *parent) :
    QObject(parent)
{
    m_pDevice = new Device(pLight->device());
    connect(m_pDevice, SIGNAL(messageReceived(QByteArray)),
            this, SLOT(receive(QByteArray)));
    m_pTimer = new QTimer(this);
    m_pTimer->setInterval(200);
    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(reTransmit()));
    rds_mode = Config::instance()->zdm()->rds();
    bus_mode = Config::instance()->zdm()->data_bus();
    waiting_OK = false;
    TXing = false;
}

Light::~Light()
{
    delete m_pDevice;
    m_pDevice = 0;

    delete m_pTimer;
    m_pTimer = 0;
}

QTimer* Light::timer()
{
    return m_pTimer;
}

Device* Light::device()
{
    return m_pDevice;
}
void Light::loadTXbuff(const QByteArray &msg){
    logger()->info("Loading Light buffer hex:%1, ascii:%2", QString(msg.toHex()), QString(msg));
    qDebug() << "Loading Light buffer " << QString(msg.toHex()) << " "  << QString(msg) << " waiting_OK = "  << waiting_OK;
    	qDebug() << "Message buff No of Entries before Push Back" <<  message_buff.size();
    message_buff.push_back(msg);
    	qDebug() << "Message buff No of Entries after Push Back" <<  message_buff.size();

        for (std::deque<QByteArray>::iterator it = message_buff.begin(); it!=message_buff.end(); ++it)
            qDebug()  << "Queue Contents"<< *it << *it->toHex();

    if (!waiting_OK)
        transmit();
}
void Light::transmit()
{
    QByteArray temp;
    waiting_OK = true;
    temp.clear();
    temp.append(message_buff.front());
    int message_type = temp.at(temp.length()-1);
    temp = temp.mid(0,temp.length()-1);//remove last byte of message
    logger()->info("Sending to Light hex:%1, ascii:%2, type:%3", QString(temp.toHex()), QString(temp), message_type);
    qDebug() << "Sending to Light... " << temp.toHex() << " "  << QString(temp) << " message_type " << message_type;
    m_pDevice->write(temp);

    if (message_type == 1){// no retries
        waiting_OK = false;
    	qDebug() << "Message buff No of Entries before pop front " <<  message_buff.size();
        message_buff.pop_front();
    	qDebug() << "Message buff No of Entries after pop front " <<  message_buff.size();
        if(message_buff.size() > 0) transmit();


    }else if(message_type == 2){
        temp.append(0x03);
    	qDebug() << "Message buff 1 No of Entries before pop front " <<  message_buff.size();
        message_buff.pop_front();
    	qDebug() << "Message buff 1 No of Entries after pop front " <<  message_buff.size();
        message_buff.push_front(temp);
    	qDebug() << "Message buff 1 No of Entries after push front " <<  message_buff.size();
        m_pTimer->start();
        m_retries = 0;
        lightMsg.clear();
        lightMsg.append(temp);// Used in Zdm::handleLightMessage to check off response from light
    }
}

void Light::reTransmit()
{
    if (m_retries >= 3)
    {
        m_pTimer->stop();
        emit noResponse(message_buff.front());
        qDebug() << "Poping up the Msg Buffer... " << message_buff.front().toHex() << " "  << QString(message_buff.front()) << " m_retries " << m_retries;
        message_buff.pop_front();
    	qDebug() << "Message buff 2 No of Entries after pop front " <<  message_buff.size();
        m_retries = 0;
        waiting_OK = false;
    }
    else
    {
        logger()->info("RESENDING to LIGHT hex:%1, ascii:%2", QString(message_buff.front().toHex()), QString(message_buff.front()));
        qDebug() << "Resending to Light... " << message_buff.front().toHex() << " "  << QString(message_buff.front()) << " m_retries " << m_retries;
        transmit();
        m_retries ++;
    }
}

void Light::receive(const QByteArray& msg)
{
    qDebug() << "Received from light  " << msg.toHex() << " " << msg;
    logger()->info("RECEIVED FROM LIGHT hex:%1, ascii:%2", QString(msg.toHex()), QString(msg));

    emit messageReceived(msg);
}
