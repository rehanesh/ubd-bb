#ifndef UTILS_H
#define UTILS_H

#include <QObject>
#include <QByteArray>
#include <log4qt/logmanager.h>

class Utils : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Utils)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit Utils(QObject* parent = 0);
    ~Utils();

    static Utils* instance();
    bool isKeyword(unsigned char c);
    void appendAdjustedValue(QByteArray& cs, int n);
    void setChecksum(QByteArray& msg);
    int verifyChecksum(const QByteArray& msg);

private:
    static Utils* m_pInstance;
    QByteArray m_keywords;
};

#endif // UTILS_H
