#ifndef LOGINDIALOG_H
#define LOGINDIALOG_H

#include <QDialog>
#include <QSignalMapper>

namespace Ui {
class LoginDialog;
}

class LoginDialog : public QDialog
{
    Q_OBJECT

public:
    explicit LoginDialog(QWidget *parent = 0);
    ~LoginDialog();

signals:
    void onloginFinished(QString value);

private slots:
    void on_pushButton_Login_clicked();
    void process_Num(QString w);

    void on_pushButton_2_clicked();

    void on_pushButton_clicked();

    void on_pushButton_Cancel_clicked();

private:
    Ui::LoginDialog *ui;
    QSignalMapper *signalMapper;
};

#endif // LOGINDIALOG_H
