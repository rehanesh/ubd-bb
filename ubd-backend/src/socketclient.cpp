#include "socketclient.h"
#include <QTimer>
#include <sys/socket.h>
#include <netinet/in.h>
#include <netinet/tcp.h>

SocketClient::SocketClient(QString ip, QString port)
{
    m_ip = ip;
    m_port = port.toLong();
    enable_hbt = false;

    //heartbeatTimer = new QTimer(this);

    connect(this, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(handleSocketError(QAbstractSocket::SocketError)));
    connect(this, SIGNAL(connected()), this, SLOT(socketConnected()));
    //connect(heartbeatTimer, SIGNAL(timeout()), this, SLOT(serverTimeout()));
    connect(this, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));

}
SocketClient::SocketClient(QString ip, QString port,bool hbt_timer){
    m_ip = ip;
    m_port = port.toLong();
     if(hbt_timer){
    enable_hbt = true;
    heartbeatTimer = new QTimer(this);
    connect(heartbeatTimer, SIGNAL(timeout()), this, SLOT(serverTimeout()));
    }

    connect(this, SIGNAL(error(QAbstractSocket::SocketError)),
            this, SLOT(handleSocketError(QAbstractSocket::SocketError)));
    connect(this, SIGNAL(connected()), this, SLOT(socketConnected()));
    connect(this, SIGNAL(disconnected()), this, SLOT(socketDisconnected()));
}


SocketClient::~SocketClient()
{
    if (this->isOpen())
    {
        this->flush();
        this->close();
    }
}

QString SocketClient::ip()
{
    return m_ip;
}

long SocketClient::port()
{
    return m_port;
}

QByteArray SocketClient::data()
{
    QByteArray data;
    if (!this->isOpen())
    {
        logger()->error("%1:%2 is not connected.",
                        m_ip, QString::number(m_port));
        opens();
    }
    if (this->bytesAvailable() != 0)
    {
         data.resize(this->bytesAvailable());
         this->read(data.data(), data.size());
         if(enable_hbt){
         //logger()->error("Starting a HRTBEAT timer");
         heartbeatTimer->start(5000);
         }

    }
    return data;
}

void SocketClient::opens()
{
    logger()->info("connecting %1:%2 ...", m_ip, QString::number(m_port));
    this->abort();
    this->connectToHost(m_ip, m_port);
}

void SocketClient::socketConnected (){
    host_connected = true;

    if(enable_hbt){
    logger()->error("Socket Connected. Starting a Heartbeat Timer");
     heartbeatTimer->start(5000);
    }
}

bool SocketClient::isConnected(){
    return host_connected;
}



void SocketClient::handleSocketError(QAbstractSocket::SocketError error)
{
    switch (error)
    {
    case QAbstractSocket::HostNotFoundError:
        logger()->error("remote host not found.");
        break;
    case QAbstractSocket::ConnectionRefusedError:
        logger()->error("connection refused by remote host.");
        break;
    case QAbstractSocket::RemoteHostClosedError:
        logger()->error("connection closed by remote host.");
        break;
    default:
        logger()->error("unkonwn error %1", this->errorString());
    }
    host_connected = false;
    QTimer::singleShot(5000, this, SLOT(opens()));
}


void SocketClient::serverTimeout () {
    logger()->error("HeartBeat Timer Expired");
    this->disconnectFromHost();
}

void SocketClient::socketDisconnected (){
    logger()->error("Client Disconnected");
    host_connected = false;
    if(enable_hbt){
    logger()->error("Stopping  Heart Beat Timer");
    heartbeatTimer->stop();
    this->opens();
    }
}
