/*******************************
 * Output GPIO signals to light up beacons
 * Author: Dongbo Tang
 * Date: 2016-01-21
 * ****************************/
#ifndef GPIO_H
#define GPIO_H

#include <QObject>
#include <QFile>
#include <QTextStream>
#include <log4qt/logmanager.h>

class Gpio : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Gpio)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit Gpio(QObject* parent = 0);
    ~Gpio();

    void greenon(); // gpio172
    void redon(); // gpio45
    void noneon();
    void output(QString path, QByteArray value);
    int input(QString path);

private:
    void turnon(QString path);
    void turnoff(QString path);
};

#endif // GPIO_H
