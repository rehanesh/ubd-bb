#include "hostGui.h"
#include <QTimer>
#include "utils.h"


HostGui::HostGui(ConfigHostGui* pHostGui, QObject* parent):
    QObject(parent)
{
    m_pTimer = new QTimer(this);
    m_pTimer->setInterval(1000);

    waiting_OK = false;

    m_pDevice = new Device(pHostGui->device());
    connect(m_pDevice, SIGNAL(SendToRDS(QByteArray)),
            this, SLOT(handleMessagesFromGui(QByteArray)) );

//    connect(m_pDevice, SIGNAL(messageReceived(QByteArray)),
//            this, SLOT(handleMessagesFromGui(QByteArray)) );

}

HostGui::~HostGui()
{
    delete m_pTimer;
    m_pTimer = 0;

    delete m_pRTSTimer;
    m_pRTSTimer = 0;

    delete m_pDevice;
    m_pDevice = 0;

    delete m_pGpio;
    m_pGpio = 0;

}

QTimer* HostGui::timer()
{
    return m_pTimer;
}

QTimer* HostGui::RTStimer()
{
    return m_pRTSTimer;
}

Device* HostGui::device()
{
    return m_pDevice;
}

void HostGui::handleRDSMessage(const QByteArray& msg)
{
    //logger()->info("RECEIVED FROM HOST Sending to Client %1",
      //             QString(msg));
    m_pDevice->SendtoGui(msg);

 }
void HostGui::handleMessagesFromGui(const QByteArray& msg)
{
    // remove ff, fe (stx and etx of the front end)
    QByteArray temp;
    logger()->info("RECEIVED FROM GUI, but make it similar to lights Sending to zdm %1   %2",QString(msg.toHex()),QString(msg));
    temp = msg.mid(1,msg.length()-2);//remove last byte of message
    logger()->info("After stripping  Sending to zdm %1   %2",QString(temp.toHex()),QString(temp));
    emit messageReceived(temp);
 }


void HostGui::displayMessage(const QString &msg)
{
    QString temp = "STX|" + msg + "|ETX";
    logger()->info("Rehanesh SENDING To GUI,  %1",temp);
    QByteArray buffer;
    buffer.append(temp);
    logger()->info("Rehanesh SENDING To GUI buffer,  %1 %2",QString(buffer.toHex()),QString(buffer));
   //QByteArray buffer = buffer.append(temp);
    m_pDevice->SendtoGui(buffer);
//    ui->label_displaymessage->setText(msg);
}

void HostGui::displayErrorMessage(const QString &msg)
{
    if(msg.isEmpty()) return;
    QString temp = "STX|" + msg + "|ETX";
    logger()->info("Rehanesh SENDING ERROR To GUI,  %1",temp);
    QByteArray buffer;
    buffer.append(temp);
    logger()->info("Rehanesh SENDING ERROR To GUI buffer,  %1 %2",QString(buffer.toHex()),QString(buffer));
    m_pDevice->SendtoGui(buffer);
  //  ui->label_displayerror->setText(msg);
}

