#ifndef TRANSLATOR_H
#define TRANSLATOR_H

#include <QObject>
#include <QByteArray>
#include <log4qt/logmanager.h>

class Translator : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Translator)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit Translator(QObject* parent = 0);
    ~Translator();
    unsigned char rds_mode;
    QString bus_mode;

    QByteArray translateHostMessage(const QByteArray& msg,
                                    unsigned char stx,
                                    unsigned char etx);
    QByteArray translateLightMessage(const QByteArray& msg);
};
#endif // TRANSLATOR_H
