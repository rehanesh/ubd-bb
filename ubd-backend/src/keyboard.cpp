#include "keyboard.h"
#include "ui_keyboard.h"
#include "config.h"
#include "stdio.h"

QColor barColor(51, 204, 102);
QColor barColorV(66, 75, 244);
QColor orange(244, 108, 4);
QPen gridPen;
QPen gridPenV;
QVector<qreal> dashes;
QColor gridColor = barColor.darker();
QColor gridColorV = barColorV.darker();

QBrush redBrush(Qt::red);
QBrush blueBrush(Qt::blue);
QBrush greenBrush(Qt::green);
QBrush yellowBrush(Qt::yellow);
QBrush grayBrush(Qt::gray);
QBrush orangeBrush(orange);
QPen blackPen(Qt::black);

QBitArray blinkState(50, false);
QBitArray blinkSled(50, false);
unsigned char butColour[50];

QString greyBut = "QPushButton {"
                  "color: black;"
                  "border: 1px solid black;"
                  "padding: 0px;"
                  "background: #e1e1e1;"
                  "max-width: 28px;"
                  "max-height: 28px;}";
QString redBut =  "QPushButton {"
                  "color: black;"
                  "border: 1px solid black;"
                  "padding: 0px;"
                  "background: red;"
                  "max-width: 28px;"
                  "max-height: 28px;}";
QString greenBut =  "QPushButton {"
                  "color: black;"
                  "border: 1px solid black;"
                  "padding: 0px;"
                  "background: green;"
                  "max-width: 28px;"
                  "max-height: 28px;}";

Keyboard::Keyboard(QWidget* parent) :
    QMainWindow(parent),
    ui(new Ui::Keyboard)
{
    ui->setupUi(this);

    gridPen.setWidth(2);
    gridPenV.setWidth(2);
    dashes << 2 << 2;
    gridPen.setDashPattern(dashes);
    gridPen.setColor(gridColor);
    gridPenV.setDashPattern(dashes);
    gridPenV.setColor(gridColorV);
    blackPen.setWidth(1);

    m_numberals = false;
    ui->button_numerals->setCheckable(true);

    QObject::connect(ui->button_numerals, SIGNAL(toggled(bool)),
                     this, SLOT(buttonNumberalsToggled(bool)));
    QObject::connect(ui->button_1, SIGNAL(clicked()),
                this, SLOT(button1Clicked()));
    QObject::connect(ui->button_2, SIGNAL(clicked()),
                this, SLOT(button2Clicked()));
    QObject::connect(ui->button_3, SIGNAL(clicked()),
                this, SLOT(button3Clicked()));
    QObject::connect(ui->button_4, SIGNAL(clicked()),
                this, SLOT(button4Clicked()));
    QObject::connect(ui->button_5, SIGNAL(clicked()),
                this, SLOT(button5Clicked()));
    QObject::connect(ui->button_6, SIGNAL(clicked()),
                this, SLOT(button6Clicked()));
    QObject::connect(ui->button_7, SIGNAL(clicked()),
                this, SLOT(button7Clicked()));
    QObject::connect(ui->button_8, SIGNAL(clicked()),
                this, SLOT(button8Clicked()));
    QObject::connect(ui->button_9, SIGNAL(clicked()),
                this, SLOT(button9Clicked()));
    QObject::connect(ui->button_0, SIGNAL(clicked()),
                this, SLOT(button0Clicked()));
    QObject::connect(ui->button_cancel, SIGNAL(clicked()),
                this, SLOT(buttonCancelClicked()));
    QObject::connect(ui->button_enter, SIGNAL(clicked()),
                this, SLOT(buttonEnterClicked()));
    QObject::connect(ui->action_quit, SIGNAL(triggered()),
                 this, SLOT(actionQuitTriggered()));
//    QObject::connect(ui->action_fullscreen, SIGNAL(triggered()),
//                this, SLOT(actionFullscreenTriggered()));
//    QObject::connect(ui->actionEnable_Maintenance_Mode, SIGNAL(triggered()),
//                this, SLOT(actionEnable_Maintenance_Mode()));

    scene = new QGraphicsScene();
//    subScene = new QGraphicsScene();
    view = new QGraphicsView();
//    subView = new QGraphicsView();
    scene->addWidget(this);

    blinkTimer = new QTimer(this);
    connect(blinkTimer, SIGNAL(timeout()), this, SLOT(upDate()));

    QSignalMapper *signalMapper = new QSignalMapper(this);
    connect(signalMapper, SIGNAL(mapped(int)), this, SLOT(digitClicked(int)));
    int hoffset;
    bool flipSide = false;
    if (Config::instance()->zdm()->flip_layout() == "yes")
        flipSide = true;
    if (flipSide)
        hoffset =24;
    else
        hoffset =0;

    int voffset =50;
    for (int x=0; x<50; x++){
        QString text = QString::number(x+1);
        btnuser[x] = new QPushButton(text, this);
        if (x == 25){
            if (flipSide)
                hoffset = 25;
            else
                hoffset = 1;
            voffset = 0;
        }
        if (x > 25)
            hoffset += 2;
        if (flipSide)
            btnuser[x]->setGeometry(QRect(2 + (hoffset-x)*32 ,100+voffset,29,29));
        else
            btnuser[x]->setGeometry(QRect(2 + (x-hoffset)*32 ,100+voffset,29,29));

        btnuser[x]->setStyleSheet(greyBut);
        int mod = x%5;
        if(mod == 0 && x < 24 && x > 1)
            scene->addLine(x*32,80,x*32,200, gridPenV);

        QObject::connect(btnuser[x], SIGNAL(clicked()),signalMapper,SLOT(map()));
        signalMapper->setMapping(btnuser[x], x);
        scene->addWidget(btnuser[x]);
    }


    view->setScene(scene);
    view->show();
    view->showFullScreen();
    view->setHorizontalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
    view->setVerticalScrollBarPolicy(Qt::ScrollBarAlwaysOff);
//    this->showFullScreen();
    this->activateWindow();
}

Keyboard::~Keyboard()
{
    delete ui;
    ui = 0;
}

void Keyboard::displayLed(int addr, int ledState){
    qDebug() << "Led Addr = " << addr << " Led State = " << ledState;
    bool blinkTest = false;
    if (ledState == 1){
        blinkSled[addr - 1] = true;
        butColour[addr - 1] = 1;
    }else if (ledState == 2) {
        blinkSled[addr - 1] = true;
        butColour[addr - 1] = 2;
    }else {
        blinkSled[addr - 1] = false;
        butColour[addr - 1] = 0;
    }
    for (int x = 0; x < 50; x++){
        if(blinkSled[x]){
            blinkTest = true;
        }
    }
    if(blinkTest)
        blinkTimer->start(255);

}
void Keyboard::upDate(){
    bool blinkTest = false;
    for (int x = 0; x < 50; x++){
        if(blinkSled[x]){
            blinkTest = true;
            if(blinkState[x]){
                btnuser[x]->setStyleSheet(greyBut);
            }else{
                if (butColour[x] == 1)
                    btnuser[x]->setStyleSheet(greenBut);
                else if (butColour[x] == 2)
                    btnuser[x]->setStyleSheet(redBut);
            }
            blinkState[x] = !blinkState[x];
        }else{
            btnuser[x]->setStyleSheet(greyBut);
        }
    }
    if(!blinkTest)
        blinkTimer->stop();

}
void Keyboard::digitClicked(int l){
    qDebug() << "BtnPress... " << l;

/*
    bool blinkTest = false;
    blinkSled[l] = !blinkSled[l];
    for (int x = 0; x < 50; x++){
        if(blinkSled[x]){
            butColour[x] = 1;
            blinkTest = true;
        }
    }
    if(blinkTest)
        blinkTimer->start(255);
*/
}
void Keyboard::displayMessage(const QString &msg)
{
    ui->label_displaymessage->setText(msg);
}

void Keyboard::displayErrorMessage(const QString &msg)
{
    ui->label_displayerror->setText(msg);
}

void Keyboard::buttonNumberalsToggled(bool checked)
{
    m_numberals = checked;
}

void Keyboard::button1Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('1');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x53, 0x54};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_start->text());
        m_inputs.clear();
    }
}

void Keyboard::button2Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('2');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x53, 0x53};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_suspend->text());
        m_inputs.clear();
    }
}

void Keyboard::button3Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('3');
        displayMessage(QString(m_inputs));
    }
    else
    {
        displayMessage("");
        m_inputs.clear();
    }
}

void Keyboard::button4Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('4');
        displayMessage(QString(m_inputs));
    }
    else
    {
        displayMessage("");
        m_inputs.clear();
    }
}

void Keyboard::button5Clicked()
{
    if (m_numberals)
    {
        m_inputs.append("5");
        displayMessage(QString(m_inputs));
    }
    else
    {
        //unsigned char data[] = {0xfd, 0x54, 0x4d};
        unsigned char data[] = {0xfd, 0x54, 0x53};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_test->text());
        m_inputs.clear();
    }
}

void Keyboard::button6Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('6');
        displayMessage(QString(m_inputs));
    }
    else
    {
        displayMessage("");
        m_inputs.clear();
    }
}

void Keyboard::button7Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('7');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x55, 0x4c};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_unload->text());
        m_inputs.clear();
    }

}

void Keyboard::button8Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('8');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x52, 0x4c};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_relight->text());
        m_inputs.clear();
    }
}

void Keyboard::button9Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('9');
        displayMessage(QString(m_inputs));
    }
    else
    {
        unsigned char data[] = {0xfd, 0x52, 0x50};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_reprn->text());
        m_inputs.clear();
    }
}

void Keyboard::button0Clicked()
{
    if (m_numberals)
    {
        m_inputs.append('0');
        displayMessage(QString(m_inputs));
    }
    else
    {
        //unsigned char data[] = {0xfd, 0x50, 0x44};
        unsigned char data[] = {0xfd, 0x44, 0x50};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->label_product->text());
        m_inputs.clear();
    }
}

void Keyboard::buttonCancelClicked()
{
    if (m_numberals)
    {
        m_numberals = false;
        ui->button_numerals->setChecked(false);
        m_inputs.clear();
        displayMessage("");
    }
    else
    {
        // cancel TEST mode -- AB
        unsigned char data[] = {0xfd, 0x41, 0x42};
        emit transmit(QByteArray::fromRawData(
                          (char*)data, sizeof(data)));
        displayMessage(ui->button_cancel->text());
        m_inputs.clear();
    }
}

void Keyboard::buttonEnterClicked()
{
    if (!m_inputs.isEmpty())
    {
        unsigned char data[] = {0xf0};
        QByteArray msg = QByteArray::fromRawData(
                    (char*)data, sizeof(data));
        msg.append(m_inputs);
        emit transmit(msg);
        displayMessage("");
        m_inputs.clear();
        ui->button_numerals->setChecked(false);
        m_numberals = false;
    }
}

void Keyboard::actionQuitTriggered()
{
    loginDialog = new LoginDialog();
    connect(loginDialog, SIGNAL(onloginFinished(QString)), this, SLOT(onDialogFinished(QString)));
    loginDialog->exec();
    delete loginDialog;
}

void Keyboard::onDialogFinished(QString value){
    if ( value == "123456" )
            emit quit();
}
