#include "zdm.h"
#include "config.h"
#include "utils.h"

Zdm::Zdm(QObject* parent) :
    QObject(parent)
{
    m_addr = Config::instance()->zdm()->addr();
    rds_mode = Config::instance()->zdm()->rds();
    bus_mode = Config::instance()->zdm()->data_bus();
    virtual_3dd = Config::instance()->zdm()->virtual3dd();
    m_pHost = 0;
    m_pPreviousSiblingZdm = 0;
    m_pNextSiblingZdm = 0;
    m_pLight = 0;
    m_pTranslator = 0;
    m_pPowerbase = 0;
    m_pConverter = 0;
    m_pScanner = 0;
    // host
    //tcp client for light messages
    if (Config::instance()->zdm()->host())
    {
        if (Config::instance()->zdm()->host()->device())
        {
            m_pHost = new Host(Config::instance()->zdm()->host());
            connect(m_pHost, SIGNAL(messageReceived(QByteArray)),
                    this, SLOT(handleHostMessage(QByteArray)));
            connect(m_pHost, SIGNAL(errorReceived(QByteArray)),
                    this, SLOT(handleErrorMessage(QByteArray)));
        }
        else
        {
            logger()->error("config.xml: host device not configured");
        }
    }
	// hostRDS
    //tcp client for GUI Display messages from RDS (DspMsg/DspList)
    if (Config::instance()->zdm()->hostRDS())
    {
        if (Config::instance()->zdm()->hostRDS()->device())
        {
            m_pHostRDS = new HostRDS(Config::instance()->zdm()->hostRDS());
        }
        else
        {
            logger()->error("config.xml: host RDS device not configured");
        }
    }

    //host gui
    //server talking to GUI client
    if (Config::instance()->zdm()->hostGui())
    {
        if (Config::instance()->zdm()->hostGui()->device())
        {
            m_pHostGui = new HostGui(Config::instance()->zdm()->hostGui());
            //signal slot for incoming messages from RDS
                        connect(m_pHostRDS, SIGNAL(GuimessageReceivedFromRDS(QByteArray)),
                                m_pHostGui, SLOT(handleRDSMessage(QByteArray)));
            //signal slot for outgoing messages to RDS
                        connect(m_pHostGui, SIGNAL(SendToRDS(QByteArray)),
                                m_pHostRDS, SLOT(transmit(QByteArray)));

            connect(m_pHostGui, SIGNAL(messageReceived(QByteArray)),
                                this, SLOT(handleKeyboardMessage(QByteArray)));

            connect(this, SIGNAL(display(QString)),
                             m_pHostGui, SLOT(displayMessage(QString)));

            connect(this, SIGNAL(displayError(QString)),
                             m_pHostGui, SLOT(displayErrorMessage(QString)));

        }
		else
        {
            logger()->error("config.xml: hostGui device not configured");
        }
    }


    // zdm
    m_addr = Config::instance()->zdm()->addr();
    if (Config::instance()->zdm()->previoussiblingzdm())
    {
        if (Config::instance()->zdm()->previoussiblingzdm()->device())
        {
            m_pPreviousSiblingZdm = new SiblingZdm(
                        Config::instance()->zdm()->previoussiblingzdm());
            connect(m_pPreviousSiblingZdm, SIGNAL(messageReceived(QByteArray)),
                    this, SLOT(handleHostMessage(QByteArray)));
        }
        else
        {
            logger()->error("config.xml: previoussiblingzdm device not configured");
        }

    }
    if (Config::instance()->zdm()->nextsiblingzdm())
    {
        if (Config::instance()->zdm()->nextsiblingzdm()->device())
        {
            m_pNextSiblingZdm = new SiblingZdm(
                        Config::instance()->zdm()->nextsiblingzdm());
            connect(m_pNextSiblingZdm, SIGNAL(messageReceived(QByteArray)),
                    this, SLOT(handleNextSiblingZdmMessage(QByteArray)));
        }
        else
        {
            logger()->error("config.xml: nextsiblingzdm device not configured");
        }
    }
    // powerbase
    if (Config::instance()->zdm()->powerbase())
    {
        if (Config::instance()->zdm()->powerbase()->device())
        {
            m_pConverter = new Converter();
            m_pPowerbase = new Powerbase(Config::instance()->zdm()->powerbase());
            connect(m_pPowerbase, SIGNAL(messageReceived(QByteArray)),
                    this, SLOT(handlePowerbaseMessage(QByteArray)));
        }
        else
        {
            logger()->error("config.xml: powerbase device not configured");
        }
    }
    // light
    if (Config::instance()->zdm()->light())
    {
        if (Config::instance()->zdm()->light()->device())
        {
            m_pTranslator = new Translator();
            m_pLight = new Light(Config::instance()->zdm()->light());
            connect(m_pLight, SIGNAL(messageReceived(QByteArray)),
                    this, SLOT(handleLightMessage(QByteArray)));
            connect(m_pLight, SIGNAL(noResponse(QByteArray)),
                    this, SLOT(handleLightNoResponse(QByteArray)));
        }
        else
        {
            logger()->error("config.xml: light device not configured");
        }
    }
    // scanner
    if (Config::instance()->zdm()->scanner())
    {
        if (Config::instance()->zdm()->scanner()->device())
        {
            m_pScanner = new Scanner(Config::instance()->zdm()->scanner());
            connect(m_pScanner, SIGNAL(messageReceived(QByteArray)),
                    this, SLOT(handleScannerMessage(QByteArray)));
        }
        else
        {
            logger()->error("config.xml: scanner device not configured");
        }
    }
    // gpio
    m_pGpio = new Gpio();
    //send startup message
    Zdm::sendReset();
     //beeper
    m_pBeeper = new Beeper();
}

Zdm::~Zdm()
{
    if (m_pHost)
    {
        delete m_pHost;
        m_pHost = 0;
    }
    if (m_pPreviousSiblingZdm)
    {
        delete m_pPreviousSiblingZdm;
        m_pPreviousSiblingZdm = 0;
    }
    if (m_pNextSiblingZdm)
    {
        delete m_pNextSiblingZdm;
        m_pNextSiblingZdm = 0;
    }
    if (m_pTranslator)
    {
        delete m_pTranslator;
        m_pTranslator = 0;
    }
    if (m_pConverter)
    {
        delete m_pConverter;
        m_pConverter = 0;
    }
    if (m_pPowerbase)
    {
        delete m_pPowerbase;
        m_pPowerbase = 0;
    }
    if (m_pLight)
    {
        delete m_pLight;
        m_pLight = 0;
    }
    if (m_pScanner)
    {
        delete m_pScanner;
        m_pScanner = 0;
    }
    delete m_pGpio;
    m_pGpio = 0;
    delete m_pBeeper;
    m_pBeeper = 0;
}

void Zdm::handleErrorMessage(const QByteArray& msg)
{
    emit displayError(msg);
}

void Zdm::handleHostMessage(const QByteArray& msg)
{
    unsigned char temp_addr = msg.at(1);
    if (m_pNextSiblingZdm && msg.size() > 1 && temp_addr != m_addr)
    {
        logger()->info("PROCESSING TO NEXT ZDM hex:%1, ascii:%2", QString(msg.toHex()), QString(msg));
        m_pNextSiblingZdm->transmit(msg);
    }
    else
    {
        logger()->info("PROCESSING MESSAGE hex:%1, ascii:%2", QString(msg.toHex()), QString(msg));
        bool isOk = false;
        bool sendOk = false;
        bool TX_next = false;
        unsigned char c;
        unsigned char nextc;
        int i = 0;
        int len = msg.size() - 1;

        int result = handleF9(msg);
        if (result == 0){//Valid F9, send OK to Host
//            if(rds_mode == 1)//Old ZDM messages
                sendOk = true;
        }else if (result > 0){//Failed F9, Discard host message
            logger()->error("Bad F9 message length");
            emit display("Bad message from Host");
            return;
        }

        while (i < len){
            c = msg.at(i);
            if (c == 0xf2){ // display on touch screen
                QString text;
                for (i=i+1; i<len; i++){
                    nextc = msg.at(i);
                    if (Utils::instance()->isKeyword(nextc)){
                        break;
                    }else{
                        text.append(nextc);
                    }
                }
                logger()->info("Rehanesh Message for FE %1", text);

                emit display(text);
            }else if (c == 0xf4){
                i++;
                if (m_pTranslator && m_pLight)
                {
                    QByteArray cs = m_pTranslator->translateHostMessage(msg,
                                m_pLight->device()->stx(),
                                m_pLight->device()->etx());
                    if (!cs.isEmpty())
                    {
                        int x;
                         //check if message is an OK
                         x = cs.indexOf(0xfd,0);
                         if (x > 0)
                         {
                             if(bus_mode == "yes" && virtual_3dd == "yes"){// there will be no OK from the 3DD
                                 if (cs.at(x+1) == 'B' && cs.at(x+2) == 'D')
                                     isOk = true;

                                 emit displayLed(cs.at(1), 0);// clear button
                                 QByteArray csTemp;
                                 csTemp.append(m_addr);
                                 csTemp.append(0x01);
                                 csTemp.append(0xf4);
                                 csTemp.append(cs.at(1));
                                 csTemp.append(0xfd);
                                 csTemp.append('O');
                                 csTemp.append('K');
                                 if(rds_mode == 2){
                                     csTemp.append(0xf9);
                                     int li = cs.indexOf(0xf9,0);
                                     csTemp.append(cs.at(li+1));
                                 }
                                 csTemp = buildHostMessage(csTemp);
                                 transmitToHost(csTemp);
                             }
                             if (cs.at(x+1) == 'O' && cs.at(x+2) == 'K')
                                 isOk = true;
                         }
                         // checking for a global message. I.E. no address or 0
                         unsigned char temp = cs.at(1);
                         if (temp > 0xef || temp == 0)
                             isOk = true;

                         x = cs.indexOf(0xf6,0);
                         if (x > 0)
                             if (cs.at(x+1) == 0x00)
                                 isOk = true;


                         if (!isOk)
                         {
//                             lightMsg.append(cs);// Used in Zdm::handleLightMessage to check off response from light
                             cs.append(0x02);//retry byte used in light.cpp
                             m_pLight->loadTXbuff(cs);
//                             m_pLight->timer()->start();
                         }else{
//                             lightMsg.clear();
                             cs.append(0x01);
                             m_pLight->loadTXbuff(cs);
                         }
                         sendOk = false;
                    }
                }
            }
            else if (c == 0xf8) // zone status beacon
            {
                i++;
                if (i < len)
                {
                    nextc = msg.at(i);
                    if (nextc == 0x10)
                    {
                        //beacon green
                        m_pGpio->greenon();
                    }
                    else
                    {
                        //beacon red
                        m_pGpio->redon();
                    }
                }
            }
            else if (c == 0xfd)
            {
                i ++;
                if (i < len)
                {
                    nextc = msg.at(i);
                    i++;
                    if (nextc == 'O') // host response OK
                    {
                        if (i < len)
                        {
                            nextc = msg.at(i);
                            if (nextc == 'K')
                            {
                                if (m_pHost->timer()->isActive())
                                {
                                    m_pHost->timer()->stop();
                                    m_pHost->RTStimer()->stop();
                                    m_pHost->waiting_OK = false;
                                    m_pHost->TXing = false;
                                    sendOk = false;
                                    m_pHost->message_buff.pop_front();
                                    handleErrorMessage("");
                                    if(m_pHost->message_buff.size() > 0)
                                        TX_next = true;

                                }
                                isOk = true;
                            }
                        }
                    }
                    else if (nextc == 'B') // beep
                    {
                        if (i < len)
                        {
                            nextc = msg.at(i);
                            if (nextc >= 0x31 && nextc <= 0x39)
                            {
                                beep(nextc-0x30); //convert nextc from char eg.'1' to integer 1
                            }
                        }
                    }
                }
            }
            else
            {
                i ++;
            }
        }
        if(sendOk){// Host OK message
            QByteArray cs;
            cs.append(m_addr);
            cs.append(0xfd);
            cs.append('O');
            cs.append('K');
            if(rds_mode == 2){//Power Base Reset
                cs.append(0xf9);
                i = msg.indexOf(0xf9,0);
                cs.append(msg.at(i+1));
            }
            cs = buildHostMessage(cs);
            transmitToHost(cs);
        }
        if (TX_next){
            m_pHost->checkRTS();// Starts next message to host
            TX_next = false;
        }
    }
}

void Zdm::handleNextSiblingZdmMessage(const QByteArray& msg)
{
    m_pHost->loadTXbuff(msg, false);
//    transmitToHost(msg);
}

int Zdm::handleF9(const QByteArray& msg)
{
    unsigned char Templength;
    unsigned char F9Length;
    int x = msg.indexOf(0xf9,1);
    if(rds_mode == 2){// New RDS (Data Bus)
        unsigned char sum = 0x00;
        int i;
        for (i=0; i<x+2; i++)//Add all bytes upto Id
        {
           sum += msg[i];
        }

        unsigned char highByte = msg[i];
        i++;
        unsigned char lowByte = msg[i];
        if(highByte >= 0x30 && highByte <= 0x39){
            highByte = highByte - 0x30;
            sum += highByte << 4;
        }else if (highByte >= 0x41 && highByte <= 0x46) {
            highByte = highByte - 0x37;
            sum += highByte << 4;
        }
        if(lowByte >= 0x30 && lowByte <= 0x39){
            sum += lowByte - 0x30;
        }else if (lowByte >= 0x41 && lowByte <= 0x46) {
            sum += lowByte - 0x37;
        }
        i++;
        sum += msg[i];

        return sum;
    }else{
        //check for f9 and message length
        if(x > 0){
            //Found F9, check message length
            F9Length = msg.at(x+1)&0x0f;// low nibble
            Templength = (msg.at(x+1) & 0xf0) >> 4;// High nibble
            F9Length += Templength*10;
            if (F9Length == x-1){
                //Valid message length
                return 0;
            }else {
                return 1;
            }
        }else{
            return -1;
        }
    }
}

void Zdm::handlePowerbaseMessage(const QByteArray &msg)
{
    logger()->info("PROCESSING MESSAGE hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));
    if (m_pConverter)
    {
        QByteArray cs = m_pConverter->convertPowerbaseMessage(msg);
        if (!cs.isEmpty())
        {
             transmitToHost(buildHostMessage(cs));
        }
    }
}

void Zdm::handleLightMessage(const QByteArray &msgin)
{
    QByteArray msg;
    msg.clear();
    msg.append(msgin);
    logger()->info("PROCESSING MESSAGE hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));
    int i;
    unsigned char c;
    unsigned char id;
    unsigned char nextc;
    bool sendOk = false;
    //int result = handleF9(msg);

    int result = handleF9(msg);
    if (result == 0){//Valid F9, send OK to Host
//            if(rds_mode == 1)//Old ZDM messages
            sendOk = true;
    }else if (result > 0){//Failed F9, Discard host message
        logger()->error("Bad F9 message length");
        emit display("Bad message from Host");
        return;
    }

    qDebug() << "Handle Light Message. Virtual 3DD... " << virtual_3dd << "Bus mode... " << bus_mode <<"HandleF9 result" << result;

    if(bus_mode == "yes"){
        qDebug() << " Rehanesh MsgIn " << msgin;
        qDebug() << " Rehanesh MshIn Addr" << msgin.at(1);
        i = msgin.indexOf(0xf9,1);
        id = msgin.at(i+1);
        if(virtual_3dd == "yes"){
            i = msgin.indexOf(0xf6,1);
            if (i == 2 && (msgin.at(i+2) == 'O' || msgin.at(i+2) == 'A')){
                QByteArray cs;
                cs.append(0xff);
                cs.append(msgin.at(1));
                if(msgin.at(i+2) == 'A'){
                    emit displayLed(msgin.at(1), 0);// clear button
                    cs.append(0xfa);
                    cs.append('\0');
                    cs.append(0x01);
                }else{
                    if (m_pLight->timer()->isActive())
                    {
                        int cAddr = m_pLight->lightMsg.indexOf(0xff,0);// checking id and address for OK message
                        unsigned char checkAddr = m_pLight->lightMsg.at(cAddr + 1);
                        if(checkAddr == msgin.at(1)){
                            int cId = m_pLight->lightMsg.indexOf(0xf9,1);
                            unsigned char checkId = m_pLight->lightMsg.at(cId + 1);
                            if(checkId == id){
                                m_pLight->timer()->stop();
                                m_pLight->waiting_OK = false;
                                m_pLight->message_buff.pop_front();
                                qDebug() << "Valid OK... waiting OK = " << m_pLight->waiting_OK;
                            }
                        }
                    }
                    emit displayLed(msgin.at(1), 1);// flash button green
                    cs.append(0xfd);
                    cs.append("O");
                    cs.append("K");
                }
                cs.append(msgin.mid(i, 2));// Adding F6 and SLED addr
                cs.append("O");
                cs.append(0xf9);
                cs.append(id);
                Utils::instance()->setChecksum(cs);
                cs.append(0xfe);
                m_pLight->lightMsg.clear();
                msg.clear();// reload msg for host translation below
                msg.append(cs);
                cs.append(0x01);//retry byte used in light.cpp
                m_pLight->loadTXbuff(cs);
            }else{
//                return;
            }
            sendOk = false;
        }else{ // NOT virtual 3DD
            if (m_pLight->timer()->isActive())
            {
                int cAddr = m_pLight->lightMsg.indexOf(0xff,0);// checking id and address for OK message
                unsigned char checkAddr = m_pLight->lightMsg.at(cAddr + 1);
                unsigned char tempAddr = (unsigned char)msgin.at(1) ;
                qDebug() << "checkADDr  " << checkAddr <<"tempAddr" << tempAddr;
                if(checkAddr == tempAddr){
                    int cId = m_pLight->lightMsg.indexOf(0xf9,1);
                    unsigned char checkId = m_pLight->lightMsg.at(cId + 1);
                    qDebug() << "checkId  " << checkId <<"id" << id;
                    if(checkId == id){
                        m_pLight->timer()->stop();
                        m_pLight->waiting_OK = false;
                        m_pLight->message_buff.pop_front();
		qDebug() << "Valid OK... waiting OK = " << m_pLight->waiting_OK;
                        sendOk = false;
                        m_pLight->lightMsg.clear();

                        if(m_pLight->message_buff.size() > 0) m_pLight->transmit();
                    }
                }
            }else{
                int x = msgin.indexOf(0xfd,1);
                if(x > 1 && msgin.at(x+1) == 'O' && msgin.at(x+2) == 'K')
                    sendOk = false;
            }
        }// End of bus mode
    }else {
        i = 0;
        int len = msgin.size();
        while (i < len)
        {
            c = msgin.at(i);
            if (c == 0xfd)
            {
                i += 2;
                nextc = msgin.at(i);
                if (msgin.at(i-1) == 'O' && nextc == 'K')
                {
                    if(bus_mode == "yes"){

                    }else {
                        if (m_pLight->timer()->isActive())
                        {
                            m_pLight->timer()->stop();
                            m_pLight->waiting_OK = false;
                            m_pLight->message_buff.pop_front();
                        }
                    }
                    sendOk = false;
                }
                break;
            }
            i ++;
        }

    }


    // Greg, need to do this in translate class
    if(sendOk){//Valid F9, send OK to 3DD
        QByteArray cs;
        cs.append(0xff);
        cs.append(msgin.at(1));
        cs.append(0xfd);
        cs.append('O');
        cs.append('K');
        if(bus_mode == "yes"){
            cs.append(0xf9);
            cs.append(id);
            Utils::instance()->setChecksum(cs);
        }else {
            cs.append(0xf9);
            Utils::instance()->appendAdjustedValue(cs, cs.size()-2);
        }
        cs.append(0xfe);
        cs.append(0x01);//retry byte used in light.cpp
        m_pLight->loadTXbuff(cs);
    }

    // send response to host
    if (m_pTranslator)
    {
        QByteArray cs;
        cs.append(m_addr);
        if(rds_mode == 2){// RDS Databus version
            cs.append(0x01);//Repeater Address
        }
        cs.append(m_pTranslator->translateLightMessage(msg));
        if (cs.size() > 1)
        {
            cs = buildHostMessage(cs);
            transmitToHost(cs);
        }
    }
}

void Zdm::handleLightNoResponse(const QByteArray& msg)
{
    logger()->error("NO RESPONSE FROM LIGHT");
    handleErrorMessage("NO RESPONSE FROM LIGHT ");

    emit displayLed(msg.at(1), 2);
    beep(3);
    int i = msg.indexOf(0xf9,1);
    unsigned char id = msg.at(i+1);
    QByteArray cs;
    cs.append(m_addr);
    if(rds_mode == 2){// RDS Databus version
        cs.append(0x01);//Repeater Address
    }    
    cs.append(0xf4);
    cs.append(msg.at(1));
    cs.append(0xfd);
    cs.append('N');
    cs.append('R');
    cs.append(0xf9);
    if(bus_mode == "yes"){
        cs.append(id);
    }

    cs = buildHostMessage(cs);
    transmitToHost(cs);
}

void Zdm::handleScannerMessage(const QByteArray& msg)
{
    logger()->info("PROCESSING MESSAGE hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));
    emit display(QString(msg));
    if (!msg.isEmpty())
    {
        QByteArray cs;
        unsigned char addr = m_addr;
        if (m_pPowerbase)
        {
            addr = m_pPowerbase->addr();
        }
        cs.append(addr);
        cs.append(msg);
        cs = buildHostMessage(cs);
        transmitToHost(cs);
    }
}

void Zdm::handleKeyboardMessage(const QByteArray& msg)
{
    logger()->info("RECEIVED FROM KEYBOARD %1, %2",
                   QString(msg.toHex()), QString(msg));
    logger()->info("PROCESSING MESSAGE hex:%1, ascii:%2",
                   QString(msg.toHex()), QString(msg));
    if (!msg.isEmpty())
    {
        QByteArray cs;
        unsigned char addr = m_addr;
        if (m_pPowerbase)
        {
            addr = m_pPowerbase->addr();
        }
        cs.append(addr);
        cs.append(msg);
        cs = buildHostMessage(cs);
        transmitToHost(cs);
    }
}

void Zdm::beep(int times)
{
    logger()->debug("BEEP %1 times", times);
    //use Gpio to drive external beeper
    m_pBeeper->play(times);
}

QByteArray Zdm::buildHostMessage(const QByteArray& msg)
{
    QByteArray cs;
    unsigned char stx;
    unsigned char etx;
    QString rts;

    if (m_pHost)
    {
        stx = m_pHost->device()->stx();
        etx = m_pHost->device()->etx();
        rts = m_pHost->device()->rtscontrol();
    }
    else if (m_pPreviousSiblingZdm)
    {
        stx = m_pPreviousSiblingZdm->device()->stx();
        etx = m_pPreviousSiblingZdm->device()->etx();
    }
    cs.append(stx);
    cs.append(msg);
    bool isOk = false;
    unsigned char c;
    unsigned char nextc;
    int i = 0;
    int len = msg.size();
    while (i < len)
    {
        c = msg.at(i);
        if (c == 0xfd)
        {
            i += 2;
            nextc = msg.at(i);
            if (msg.at(i-1) == 'O' && nextc == 'K')
            {
                isOk = true;
            }
            break;
        }
        i ++;
    }
    if(rds_mode == 2){
        Utils::instance()->setChecksum(cs);
    }else{
        if (!isOk){
            cs.append(0xf9);
            Utils::instance()->appendAdjustedValue(cs, cs.size()-2);
        }
    }

    cs.append(etx);
    return cs;
}

void Zdm::transmitToHost(const QByteArray& msg)
{
    if (m_pHost)
    {
        m_pHost->loadTXbuff(msg, true);
    }
/*    if (m_pPreviousSiblingZdm)
    {
        m_pPreviousSiblingZdm->checkRTS(msg);
    }*/
}

void Zdm::sendReset(){
    QByteArray cs;
    cs.append(m_addr);
    cs.append(0xfd);
    if(rds_mode == 2){//Power Base Reset
        cs.append('R');
        cs.append('M');
        cs.append(0xf9);
        cs.append(0x01);
    }else{// ZDM Reset
        cs.append('Z');
        cs.append('R');
    }
    cs = buildHostMessage(cs);
    transmitToHost(cs);
}
