#ifndef REPEATER_H
#define REPEATER_H

#include "config.h"
#include <QObject>
#include <log4qt/logmanager.h>

class Repeater : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Repeater)
    LOG4QT_DECLARE_QCLASS_LOGGER

public:
    explicit Repeater(ConfigRepeater* pRepeater, QObject* parent = 0);
    ~Repeater();

    unsigned char addr();

private:
    unsigned char m_addr;
};

#endif // REPEATER_H
