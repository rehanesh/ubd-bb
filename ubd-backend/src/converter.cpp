#include "converter.h"
#include "utils.h"
#include <QList>
#include <QApplication>

Converter::Converter(QObject* parent) :
    QObject(parent)
{
}

Converter::~Converter()
{
}

QByteArray Converter::convertHostMessage(const QByteArray& msg,
                                         unsigned char stx,
                                         unsigned char etx,
                                         int raddr,
                                         int packetId)
{
    logger()->debug("CONVERTING HOST MESSAGE hex:%1, ascii:%2",
                    QString(msg.toHex()), QString(msg));
    int msg_head_len = 0;
    QByteArray cs;
    unsigned char c;
    unsigned char nextc;
    int i = 0;
    int len = msg.size()-1;
    while (i<len)
    {
        c = msg.at(i);
        if (i == 0)
        {
            cs.append(stx);
            for (i=i+1; i<len; i++)
            {
                nextc = msg.at(i);
                if (Utils::instance()->isKeyword(nextc))
                {
                    cs.append(raddr); // repeater addr
                    msg_head_len = cs.size();
                    break;
                }
                else
                {
                    cs.append(nextc);
                }
            }
        }
        else if (c == 0xf0 || c == 0xf1)
        {
            cs.append(c);
            for (i=i+1; i<len; i++)
            {
                nextc = msg.at(i);
                if (Utils::instance()->isKeyword(nextc))
                {
                    break;
                }
                else
                {
                    cs.append(nextc);
                }
            }
        }
        else if (c == 0xf4 || c == 0xf6)
        {
            cs.append(c);
            i ++;
            if (i < len)
            {
                nextc = msg.at(i);
                if (Utils::instance()->isKeyword(nextc))
                {
                    nextc = 0x00;
                }
                cs.append(nextc);
            }
        }
        else if (c == 0xfd)
        {
            i += 2;
            if (i < len)
            {
                nextc = msg.at(i);
                if (msg.at(i-1) == 'B') // skip beep
                {
                    if (!(nextc <= 0x39 && nextc >= 0x31)) // skip beep
                    {
                        cs.append(c);
                        cs.append('B');
                        if (nextc == 'A')
                        {
                            cs.append('D');
                        }
                        else
                        {
                            cs.append(nextc);
                        }
                    }
                }
                else
                {
                    cs.append(c);
                    for (i=i-1; i<len; i++)
                    {
                        nextc = msg.at(i);
                        if (Utils::instance()->isKeyword(nextc))
                        {
                            break;
                        }
                        else
                        {
                            cs.append(nextc);
                        }
                    }
                }
            }
        }
        else
        {
            i ++;
        }
    }
    if (cs.size() > msg_head_len)
    {
        cs.append(0xf9);
        cs.append(packetId);
        appendCheckSum(cs, stx, etx);
        cs.append(etx);
    }
    else
    {
        cs.clear();
    }
    logger()->debug("RETURN MESSAGE hex:%1, ascii:%2",
                    QString(cs.toHex()), QString(cs));
    return cs;
}

QByteArray Converter::convertPowerbaseMessage(const QByteArray& msg)
{
    logger()->debug("CONVERTING MESSAGE hex:%1, ascii:%2",
                    QString(msg.toHex()),QString(msg));
    int msg_head_len = 0;
    QByteArray cs;
    unsigned char c;
    unsigned char nextc;
    int i = 0;
    int len = msg.size()-1;
    while (i<len)
    {
        c = msg.at(i);
        if (i == 0)
        {
            //cs.append(c);
            i ++;
            if (i<len)
            {
                nextc = msg.at(i);
                if (!Utils::instance()->isKeyword(nextc))
                {
                    cs.append(nextc); // powerbase addr
                    for (i=i+1; i<len; i++)
                    {
                        nextc = msg.at(i);
                        if (Utils::instance()->isKeyword(nextc))
                        {
                            msg_head_len = cs.size();
                            break;
                        }
                        // removed repeater addr ...
                    }
                }
            }
        }
        else if (c == 0xf4 || c == 0xf6 || c == 0xfb)
        {
            cs.append(c);
            for (i=i+1; i<len; i++)
            {
                nextc = msg.at(i);
                if (!Utils::instance()->isKeyword(nextc))
                {
                    cs.append(nextc);
                }
                else
                {
                    break;
                }
            }
        }
        else if (c == 0xf9)
        {
            break;
        }
        else if (c == 0xfa)
        {
            cs.append(c);
            for (i=i+2; i<len; i++) // FA 00 01 > FA 01
            {
                nextc = msg.at(i);
                if (!Utils::instance()->isKeyword(nextc))
                {
                    cs.append(nextc);
                }
                else
                {
                    break;
                }
            }
        }
        else if (c == 0xfd)
        {
            cs.append(c);
            i ++;
            if (i < len)
            {
                nextc = msg.at(i);
                if (!Utils::instance()->isKeyword(nextc))
                {
                    if (nextc == 'R')
                    {
                        logger()->debug("convert reset message RM RP RB to ZR");
                        cs.append('Z');
                        cs.append('R');
                        i ++;
                    }
                    else
                    {
                        cs.append(nextc);
                        for (i=i+1; i<len; i++)
                        {
                            nextc = msg.at(i);
                            if (!Utils::instance()->isKeyword(nextc))
                            {
                                cs.append(nextc);
                            }
                            else
                            {
                                break;
                            }
                        }
                    }
                }
            }
        }
        else
        {
            i ++;
        }
    }
    if (cs.size() <= msg_head_len)
    {
        cs.clear();
    }
    logger()->debug("RETURN MESSAGE hex:%1, ascii:%2",
                    QString(cs.toHex()), QString(cs));
    return cs;
}

void Converter::appendCheckSum(QByteArray& cs,
                               unsigned char stx,
                               unsigned char etx)
{
    unsigned char* data = (unsigned char*)(cs.data());
    unsigned char sum = stx;
    int i;
    for (i=1; i<cs.size(); i++)
    {
       sum += data[i];
    }
    sum += etx;
    sum = stx - sum + 1;
    unsigned char temp = sum >> 4;
    if (temp > 9)
    {
        cs.append(temp+0x37);
    }
    else
    {
        cs.append(temp+0x30);
    }
    temp = sum & 0x0f;
    if (temp > 9)
    {
        cs.append(temp+0x37);
    }
    else
    {
        cs.append(temp+0x30);
    }
}
