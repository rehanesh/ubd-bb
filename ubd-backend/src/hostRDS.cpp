#include "hostRDS.h"
#include <QTimer>


HostRDS::HostRDS(ConfigHostRDS* pHostRDS, QObject* parent):
    QObject(parent)
{
    m_pTimer = new QTimer(this);
    m_pTimer->setInterval(5000);
    m_pRTSTimer = new QTimer(this);
    m_pRTSTimer->setInterval(20);
    m_pGpio = new Gpio();
    waiting_OK = false;
    TXing = false;

    connect(m_pTimer, SIGNAL(timeout()), this, SLOT(reTransmit()));
    connect(m_pRTSTimer, SIGNAL(timeout()), this, SLOT(checkCTS()));

    m_pDevice = new Device(pHostRDS->device());
    connect(m_pDevice, SIGNAL(GuimessageReceivedFromRDS(QByteArray)),
            this, SLOT(receive(QByteArray)) );

}

HostRDS::~HostRDS()
{
    delete m_pTimer;
    m_pTimer = 0;

    delete m_pRTSTimer;
    m_pRTSTimer = 0;

    delete m_pDevice;
    m_pDevice = 0;

    delete m_pGpio;
    m_pGpio = 0;

}

QTimer* HostRDS::timer()
{
    return m_pTimer;
}

QTimer* HostRDS::RTStimer()
{
    return m_pRTSTimer;
}

Device* HostRDS::device()
{
    return m_pDevice;
}
void HostRDS::receive(const QByteArray& msg)
{
    //logger()->info("RECEIVED FROM HostRDS %1",
      //             QString(msg));
    //sending signal to hostGUI
    emit GuimessageReceivedFromRDS(msg);
}

void HostRDS::transmit(const QByteArray& msg)
{
    //logger()->info("RECEIVED FROM GUI %1",
      //              QString(msg));
    //need to send the message to Server.
    m_pDevice->write(msg);
}
