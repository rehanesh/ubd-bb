/****************************************************************************
** Meta object code from reading C++ file 'keyboard.h'
**
** Created: Mon Oct 22 23:04:05 2018
**      by: The Qt Meta Object Compiler version 63 (Qt 4.8.2)
**
** WARNING! All changes made in this file will be lost!
*****************************************************************************/

#include "keyboard.h"
#if !defined(Q_MOC_OUTPUT_REVISION)
#error "The header file 'keyboard.h' doesn't include <QObject>."
#elif Q_MOC_OUTPUT_REVISION != 63
#error "This file was generated using the moc from 4.8.2. It"
#error "cannot be used with the include files from this version of Qt."
#error "(The moc has changed too much.)"
#endif

QT_BEGIN_MOC_NAMESPACE
static const uint qt_meta_data_Keyboard[] = {

 // content:
       6,       // revision
       0,       // classname
       0,    0, // classinfo
      22,   14, // methods
       0,    0, // properties
       0,    0, // enums/sets
       0,    0, // constructors
       0,       // flags
       2,       // signalCount

 // signals: signature, parameters, type, tag, flags
       9,   30,   34,   34, 0x05,
      35,   34,   34,   34, 0x05,

 // slots: signature, parameters, type, tag, flags
      42,   30,   34,   34, 0x0a,
      66,   30,   34,   34, 0x0a,
      95,  115,   34,   34, 0x0a,
     129,  147,   34,   34, 0x0a,
     149,   34,   34,   34, 0x0a,
     158,  187,   34,   34, 0x08,
     195,   34,   34,   34, 0x08,
     212,   34,   34,   34, 0x08,
     229,   34,   34,   34, 0x08,
     246,   34,   34,   34, 0x08,
     263,   34,   34,   34, 0x08,
     280,   34,   34,   34, 0x08,
     297,   34,   34,   34, 0x08,
     314,   34,   34,   34, 0x08,
     331,   34,   34,   34, 0x08,
     348,   34,   34,   34, 0x08,
     365,   34,   34,   34, 0x08,
     387,   34,   34,   34, 0x08,
     408,   34,   34,   34, 0x08,
     430,  456,   34,   34, 0x08,

       0        // eod
};

static const char qt_meta_stringdata_Keyboard[] = {
    "Keyboard\0transmit(QByteArray)\0msg\0\0"
    "quit()\0displayMessage(QString)\0"
    "displayErrorMessage(QString)\0"
    "displayLed(int,int)\0addr,ledState\0"
    "digitClicked(int)\0l\0upDate()\0"
    "buttonNumberalsToggled(bool)\0checked\0"
    "button1Clicked()\0button2Clicked()\0"
    "button3Clicked()\0button4Clicked()\0"
    "button5Clicked()\0button6Clicked()\0"
    "button7Clicked()\0button8Clicked()\0"
    "button9Clicked()\0button0Clicked()\0"
    "buttonCancelClicked()\0buttonEnterClicked()\0"
    "actionQuitTriggered()\0onDialogFinished(QString)\0"
    "value\0"
};

void Keyboard::qt_static_metacall(QObject *_o, QMetaObject::Call _c, int _id, void **_a)
{
    if (_c == QMetaObject::InvokeMetaMethod) {
        Q_ASSERT(staticMetaObject.cast(_o));
        Keyboard *_t = static_cast<Keyboard *>(_o);
        switch (_id) {
        case 0: _t->transmit((*reinterpret_cast< const QByteArray(*)>(_a[1]))); break;
        case 1: _t->quit(); break;
        case 2: _t->displayMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 3: _t->displayErrorMessage((*reinterpret_cast< const QString(*)>(_a[1]))); break;
        case 4: _t->displayLed((*reinterpret_cast< int(*)>(_a[1])),(*reinterpret_cast< int(*)>(_a[2]))); break;
        case 5: _t->digitClicked((*reinterpret_cast< int(*)>(_a[1]))); break;
        case 6: _t->upDate(); break;
        case 7: _t->buttonNumberalsToggled((*reinterpret_cast< bool(*)>(_a[1]))); break;
        case 8: _t->button1Clicked(); break;
        case 9: _t->button2Clicked(); break;
        case 10: _t->button3Clicked(); break;
        case 11: _t->button4Clicked(); break;
        case 12: _t->button5Clicked(); break;
        case 13: _t->button6Clicked(); break;
        case 14: _t->button7Clicked(); break;
        case 15: _t->button8Clicked(); break;
        case 16: _t->button9Clicked(); break;
        case 17: _t->button0Clicked(); break;
        case 18: _t->buttonCancelClicked(); break;
        case 19: _t->buttonEnterClicked(); break;
        case 20: _t->actionQuitTriggered(); break;
        case 21: _t->onDialogFinished((*reinterpret_cast< QString(*)>(_a[1]))); break;
        default: ;
        }
    }
}

const QMetaObjectExtraData Keyboard::staticMetaObjectExtraData = {
    0,  qt_static_metacall 
};

const QMetaObject Keyboard::staticMetaObject = {
    { &QMainWindow::staticMetaObject, qt_meta_stringdata_Keyboard,
      qt_meta_data_Keyboard, &staticMetaObjectExtraData }
};

#ifdef Q_NO_DATA_RELOCATION
const QMetaObject &Keyboard::getStaticMetaObject() { return staticMetaObject; }
#endif //Q_NO_DATA_RELOCATION

const QMetaObject *Keyboard::metaObject() const
{
    return QObject::d_ptr->metaObject ? QObject::d_ptr->metaObject : &staticMetaObject;
}

void *Keyboard::qt_metacast(const char *_clname)
{
    if (!_clname) return 0;
    if (!strcmp(_clname, qt_meta_stringdata_Keyboard))
        return static_cast<void*>(const_cast< Keyboard*>(this));
    return QMainWindow::qt_metacast(_clname);
}

int Keyboard::qt_metacall(QMetaObject::Call _c, int _id, void **_a)
{
    _id = QMainWindow::qt_metacall(_c, _id, _a);
    if (_id < 0)
        return _id;
    if (_c == QMetaObject::InvokeMetaMethod) {
        if (_id < 22)
            qt_static_metacall(this, _c, _id, _a);
        _id -= 22;
    }
    return _id;
}

// SIGNAL 0
void Keyboard::transmit(const QByteArray & _t1)
{
    void *_a[] = { 0, const_cast<void*>(reinterpret_cast<const void*>(&_t1)) };
    QMetaObject::activate(this, &staticMetaObject, 0, _a);
}

// SIGNAL 1
void Keyboard::quit()
{
    QMetaObject::activate(this, &staticMetaObject, 1, 0);
}
QT_END_MOC_NAMESPACE
