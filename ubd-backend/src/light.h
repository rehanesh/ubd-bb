#ifndef LIGHT_H
#define LIGHT_H

#include "config.h"
#include "device.h"
#include <QObject>
#include <QTimer>
#include <QByteArray>
#include <deque>
#include <log4qt/logmanager.h>

class Light : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Light)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit Light(ConfigLight* pLight, QObject *parent = 0);
    ~Light();

    Device* device();
    QTimer* timer();
    unsigned char rds_mode;
    QString bus_mode;
    bool waiting_OK;
    bool TXing;
    std::deque <QByteArray> message_buff;
    void loadTXbuff(const QByteArray& msg);
    void transmit();
    QByteArray lightMsg;

signals:
    void messageReceived(const QByteArray& msg);
    void noResponse(const QByteArray& daddr);

private slots:
    void receive(const QByteArray& msg);
    void reTransmit();

private:
    Device* m_pDevice;
    QTimer* m_pTimer;
    QByteArray m_message;
    int m_retries;
};

#endif // LIGHT_H
