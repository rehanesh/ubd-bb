#ifndef SCANNER_H
#define SCANNER_H

#include "config.h"
#include "device.h"

#include <QObject>
#include <QByteArray>
#include <log4qt/logger.h>

class Scanner : public QObject
{
    Q_OBJECT
    LOG4QT_DECLARE_QCLASS_LOGGER
    Q_DISABLE_COPY(Scanner)

public:
    explicit Scanner(ConfigScanner* pScanner, QObject* parent = 0);
    ~Scanner();

signals:
    void messageReceived(const QByteArray& msg);

private slots:
    void receive(const QByteArray& msg);

private:
    Device* m_pDevice;
};

#endif // SCANNER_H
