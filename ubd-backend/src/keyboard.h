#ifndef KEYBOARD_H
#define KEYBOARD_H
#include "logindialog.h"

#include <QMainWindow>
#include <QWidget>
#include <QByteArray>
#include <QString>
#include <QGraphicsWidget>
#include <QGraphicsView>
#include <QObject>
#include <QPushButton>
#include <QSignalMapper>
#include <QGridLayout>
#include <QMessageBox>
#include <QDebug>
#include <QTimer>
#include <QBitArray>

namespace Ui {
class Keyboard;
}

class Keyboard : public QMainWindow
{
    Q_OBJECT
    Q_DISABLE_COPY(Keyboard)

public:
    explicit Keyboard(QWidget *parent = 0);
    ~Keyboard();

    int rotate_angle;
    QGraphicsScene *scene;
//    QGraphicsScene *subScene;
    QGraphicsView *view;
//    QGraphicsView *subView;
//    QPushButton *subBut;
    QPushButton *btnuser[52];
    QTimer *blinkTimer;

signals:
    void transmit(const QByteArray& msg);
    void quit();

public slots:
    void displayMessage(const QString& msg);
    void displayErrorMessage(const QString& msg);
    void displayLed(int addr, int ledState);
    void digitClicked(int l);
    void upDate();

private slots:
    void buttonNumberalsToggled(bool checked);
    void button1Clicked();
    void button2Clicked();
    void button3Clicked();
    void button4Clicked();
    void button5Clicked();
    void button6Clicked();
    void button7Clicked();
    void button8Clicked();
    void button9Clicked();
    void button0Clicked();
    void buttonCancelClicked();
    void buttonEnterClicked();
    void actionQuitTriggered();
    void onDialogFinished(QString value);
//    void actionFullscreenTriggered();

private:
    Ui::Keyboard* ui;
    LoginDialog *loginDialog;
    QByteArray m_inputs;
    bool m_numberals;
};

#endif // KEYBOARD_H
