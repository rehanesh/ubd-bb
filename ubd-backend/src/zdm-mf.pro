include(../lib/qextserialport/src/qextserialport.pri)
include(../lib/log4qt-master/src/log4qt/log4qt.pri)
QT       += core
QT       += xml
QT       += network


greaterThan(QT_MAJOR_VERSION, 4): QT += widgets

TARGET = zdm-mf
TEMPLATE = app


SOURCES += main.cpp\
    #keyboard.cpp \
    zdm.cpp \
    host.cpp \
    translator.cpp \
    siblingzdm.cpp \
    light.cpp \
    converter.cpp \
    powerbase.cpp \
    repeater.cpp \
    scanner.cpp \
    device.cpp \
    serialport.cpp \
    socketclient.cpp \
    gpio.cpp \
    utils.cpp \
    config.cpp \
    logindialog.cpp \
    socketserver.cpp \
    hostGui.cpp \
    hostRDS.cpp \
    beeper.cpp

#HEADERS  += keyboard.h \
HEADERS  += zdm.h \
    host.h \
    translator.h \
    siblingzdm.h \
    light.h \
    converter.h \
    powerbase.h \
    repeater.h \
    scanner.h \
    device.h \
    serialport.h \
    socketclient.h \
    gpio.h \
    utils.h \
    config.h \
    logindialog.h \
    socketserver.h \
    hostGui.h \
    hostRDS.h \
    beeper.h

#FORMS    += keyboard.ui \
 #   logindialog.ui
