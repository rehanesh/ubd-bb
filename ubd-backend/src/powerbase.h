#ifndef POWERBASE_H
#define POWERBASE_H

#include "config.h"
#include "device.h"
#include "repeater.h"
#include <QObject>
#include <QString>
#include <QByteArray>
#include <log4qt/logmanager.h>

class Powerbase : public QObject
{
    Q_OBJECT
    Q_DISABLE_COPY(Powerbase)
    LOG4QT_DECLARE_QCLASS_LOGGER
public:
    explicit Powerbase(ConfigPowerbase* pPowerbase, QObject* parent = 0);
    ~Powerbase();

    unsigned char addr();
    unsigned char packetId();
    Device* device();
    Repeater* repeater();
    void transmit(const QByteArray& msg);

signals:
    void messageReceived(const QByteArray& msg);

private slots:
    void receive(const QByteArray& msg);

private:
    unsigned char m_addr;
    unsigned char m_packetId;
    Repeater* m_pRepeater;
    Device* m_pDevice;
};

#endif // POWERBASE_H
